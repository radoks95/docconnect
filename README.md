# DocConnect

## Run Locally

In order to start the project make sure that you have Node.js on your machine. It is essential, since you will need the Node Package Manager (npm) in order to install the necessary dependencies and run the project locally. You can check if you have Node.js by opening a terminal and running `node -v`. If you don't have Node.js installed on your machine go to the official site - https://nodejs.org/en and download and install the recommended for most users version. If installed successfully you will see the version that you have by running `node -v` and you will be good to proceed from here.

Clone the project

```bash
  git clone https://gitlab.mentormate.bg/base/mmu/foundation/green-squad-project/frontend.git
```

Switch to the development branch

```bash
  git checkout development
```

Go to the project directory

```bash
  cd DocConnect
```

Install dependencies on root level

```bash
  npm install
```

Start the app

```bash
  npm run dev
```


## Production Build

Create a Production Build

```bash
  npm run build
```
This will create a bundle and optimize the application for production. A `dist` folder will be generated.
