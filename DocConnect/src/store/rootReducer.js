import {combineReducers} from '@reduxjs/toolkit';
import specialtiesReducer from '../slices/specialtiesSlice';
import specialistsReducer from '../slices/specialistsSlice';
import upcomingAppointmentsReducer from '../slices/upcomingAppointmentSlice';
import pastAppointmentsReducer from '../slices/pastAppointmentsSlice';
import citiesReducer from '../slices/citiesSlice';
import userReducer from '../slices/userSlice';


const rootReducer = combineReducers({
  specialties: specialtiesReducer,
  specialists: specialistsReducer,
  user: userReducer,
  cities: citiesReducer,
  upcomingAppointments: upcomingAppointmentsReducer,
  pastAppointments: pastAppointmentsReducer,
});

export default rootReducer;
