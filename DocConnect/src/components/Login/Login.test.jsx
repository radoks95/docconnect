/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import {ChakraProvider} from '@chakra-ui/react';
import Login from './Login';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import store from '../../store/store';

// Mock matchMedia
global.matchMedia = global.matchMedia || function() {
  return {
    addListener: vi.fn(),
    removeListener: vi.fn(),
  };
};

describe('Login component', () => {
  it('should submit the form with valid data', () => {
    const formikMock = {
      values: {
        email: 'test@example.com',
        password: 'P@ssw0rd',
      },
      touched: {
        email: true,
        password: true,
      },
      errors: {},
      handleBlur: vi.fn(),
      handleChange: vi.fn(),
      handleSubmit: vi.fn(),
    };

    render(
        <ChakraProvider>
          <Provider store={store}>
            <BrowserRouter>
              <Login formik={formikMock} />
            </BrowserRouter>
          </Provider>
        </ChakraProvider>,
    );


    const submitButton = screen.getByRole('button', {name: 'Login'});

    fireEvent.click(submitButton);

    expect(formikMock.handleSubmit).toHaveBeenCalled();
  });
});

it('should display error messages for invalid fields', () => {
  const formikMock = {
    values: {
      email: '',
      password: '',
    },
    touched: {
      email: true,
      password: true,
    },
    errors: {
      email: 'Please enter your email address.',
      password: 'Please enter a password.',
    },
    handleBlur: vi.fn(),
    handleChange: vi.fn(),
    handleSubmit: vi.fn(),
  };

  render(
      <ChakraProvider>
        <Provider store={store}>
          <BrowserRouter>
            <Login formik={formikMock} />
          </BrowserRouter>
        </Provider>
      </ChakraProvider>,
  );

  const submitButton = screen.getByRole('button', {name: 'Login'});

  fireEvent.click(submitButton);

  expect(formikMock.handleSubmit).toHaveBeenCalled();

  expect(screen.getByText('Please enter your email address.')).toBeInTheDocument();
  expect(screen.getByText('Please enter a password.')).toBeInTheDocument();
});
