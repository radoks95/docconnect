import {
  Box,
  Heading,
  Text,
  Input,
  Button,
  FormControl,
  FormLabel,
  useBreakpointValue,
  FormErrorMessage,
} from '@chakra-ui/react';
import {Link} from 'react-router-dom';
import LoginTheme from '../../theme/LoginTheme';
import {SIGN_UP_PAGE, FORGOT_PASSWORD} from '../../common/routes';
import PasswordInput from '../../components/PasswordInput/PasswordInput';
import ServerErrorMessage from '../ServerErrorMessage/ServerErrorMessage';

const Login = ({formik, serverError, handleResendVerification}) => {
  const isMobile = useBreakpointValue({base: true, md: false}, {ssr: false});

  const notConfirmedEmail = serverError && serverError.includes('Please confirm your email before trying to login');

  return (
    <Box
      as="form"
      __css={LoginTheme.baseStyle}
      p={isMobile ? '1.4rem 2.3rem' : '2.4rem 4.3rem'}
      onSubmit={formik.handleSubmit}
    >
      <Heading alignSelf="stretch" fontSize="1.5rem">
        Login
      </Heading>

      {serverError && <ServerErrorMessage text={serverError} width={'100%'} maxWidth={'17rem'} /> }


      {!notConfirmedEmail ? (
        <>
          <Box gap="20px" display='flex' flexDirection="column" w='100%'>
            <FormControl
              __css={LoginTheme.boxStyle}
              isInvalid={formik.touched.email && formik.errors.email}
            >
              <FormLabel fontSize="14px" htmlFor='email'>Email Address *</FormLabel>
              <Input
                placeholder="placeholder@email.com"
                h="2.4rem"
                id="email"
                name="email"
                type="email"
                focusBorderColor='green.500'
                value={formik.values.email}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
              />
              {(formik.touched.email && formik.errors.email) &&
                (<FormErrorMessage color='red.600'>{formik.errors.email}</FormErrorMessage>)}
            </FormControl>

            <FormControl __css={LoginTheme.boxStyle} isInvalid={formik.errors.password && formik.touched.password}>
              <FormLabel fontSize="14px" htmlFor='password'>Password *</FormLabel>
              <PasswordInput
                id={'password'}
                value={formik.values.password}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange} />
              {(formik.touched.password && formik.errors.password) && (
                <FormErrorMessage color='red.600'>{formik.errors.password}</FormErrorMessage>
              )}
            </FormControl>
            <Text
              color="green.500"
              fontSize="1rem"
              mt='0.7rem'
              lineHeight="1.5rem"
              letterSpacing="0.15px"
              textDecoration="underline"
              textAlign="right"
            >
              <Link to={FORGOT_PASSWORD}>
            Forgot Password?
              </Link>
            </Text>
            <Button __css={LoginTheme.buttonStyle} type='submit'>Login</Button>
          </Box>
          <Box
            display="flex"
            gap="5px"
            flexDirection="row"
            w="100%"
            justifyContent="center"
          >
            <Text fontSize="1rem" lineHeight="1.5rem" letterSpacing="0.15px">
          Don't have an account?
            </Text>
            <Link to={SIGN_UP_PAGE}>
              <Text
                as="span"
                color="green.500"
                fontSize="1rem"
                lineHeight="1.5rem"
                letterSpacing="0.15px"
                textDecoration="underline"
              >
            Sign Up
              </Text>
            </Link>
          </Box>
        </>
        ) : (
          <Button __css={LoginTheme.buttonStyle} onClick={handleResendVerification}>Request a new email</Button>
        )}

    </Box>
  );
};

export default Login;
