import {
  Tabs,
  TabList,
  Tab,
  TabPanels,
  Box,
  TabIndicator,
} from '@chakra-ui/react';
import AppointmentsTabPanel from '../AppointmentsTabPannel/AppointmentsTabPanel';
import {useUpcomingAppointments} from '../../hooks/useUpcomingAppointments';
import {usePastAppointments} from '../../hooks/usePastAppointments';
import {useSelector} from 'react-redux';

const AppointmentsTable = () => {
  const user = useSelector((state) => state.user.user);
  const {upcomingAppointments} = useUpcomingAppointments(user.id);
  const {pastAppointments} = usePastAppointments(user.id);

  return (
    <Box w="87vw" m='0 auto'>
      <Tabs
        position='relative'
        isFitted
        variant="unstyled"
        bg="white"
        minH="50vh"
        overflow="hidden"
        borderRadius='1rem'
        boxShadow='2px 2px 6px 0px rgba(0, 0, 0, 0.1)'>
        <TabList mb='0.5rem'>
          <Tab fontSize='18px'color='gray.600'
            _selected={{color: 'green.700', fontSize: '20px'}}>Upcoming appointments</Tab>
          <Tab fontSize='18px'color='gray.600'
            _selected={{color: 'green.700', fontSize: '20px'}}>Past appointments</Tab>
        </TabList>
        <TabIndicator
          mt="-1.5px"
          height="3px"
          bg="green.500"
          borderRadius="1px"/>
        <TabPanels>
          <AppointmentsTabPanel appointments={upcomingAppointments} />
          <AppointmentsTabPanel appointments={pastAppointments} hideCancelButton={true} />
        </TabPanels>
      </Tabs>
    </Box>
  );
};

export default AppointmentsTable;
