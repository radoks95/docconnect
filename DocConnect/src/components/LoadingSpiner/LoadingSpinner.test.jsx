/* eslint-disable no-undef */
import {render, screen} from '@testing-library/react';
import LoadingSpiner from './LoadingSpiner';

describe('LoadingSpiner component', () => {
  it('renders a spinner element', () => {
    render(<LoadingSpiner />);

    const spinnerElement = screen.getByText('Loading...');
    expect(spinnerElement).toBeInTheDocument();
  });
});
