
import {Spinner} from '@chakra-ui/react';


const LoadingSpiner = () => {
  return (
    <Spinner
      thickness='4px'
      speed='0.65s'
      emptyColor='green.600'
      color='green.500'
      size='xl'
    />
  );
};

export default LoadingSpiner;
