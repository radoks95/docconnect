import {Box, Image, Heading, Text, Img} from '@chakra-ui/react';
import SpecialistCardTheme from '../../theme/SpecialistCardTheme';
import {NavLink} from 'react-router-dom';
import locationSvg from '../../../public/location.svg';
import starSvg from '../../../public/star.svg';
import ScheduleAppointmentModal from '../ScheduleAppointmentModal/ScheduleAppointmentModal';

const SpecialistCard = ({specialist: {fullName, imageUrl, specialityName, location, id}}) => {
  return (

    <Box
      __css={SpecialistCardTheme.baseStyle}>
      <NavLink to={`/specialists/profile/${id}`}>
        <Box position="relative" display="inline-block">
          <Image src={imageUrl} alt={fullName} w="25rem" h="21rem" objectFit="cover" objectPosition=" left center"/>
          <Box __css={SpecialistCardTheme.imageBox}/>
        </Box>
        <Box __css={SpecialistCardTheme.headingBox} color= "green.800">
          <Heading fontSize="24px">
            {fullName}
          </Heading>
          <Box display='flex' justifyContent='space-between' flexDirection='row' w='100%'>
            <Text color="green.600"
              __css={SpecialistCardTheme.text}>
              {specialityName}
            </Text>

            {/* <Box display='flex' flexDirection='row' gap='2px'>
            <Img src={starSvg} />
            <Text>4.1</Text>
          </Box> */}

          </Box>
        </Box>
        <Box __css={SpecialistCardTheme.locationBox}>
          <Img src={locationSvg} />
          <Text color= "green.800"
            __css={SpecialistCardTheme.text}>
            {location}
          </Text>
        </Box>

      </NavLink>
      <Box w='100%' alignItems='center' px='1.6rem' my='1.3rem'>
        <ScheduleAppointmentModal id={id} />
      </Box>
    </Box>

  );
};

export default SpecialistCard;
