import {Container} from '@chakra-ui/react';

const Layout = ({children}) => {
  return (
    <Container
      display='flex'
      alignItems='center'
      justifyContent='center'
      maxW="100%"
      minH='calc(100vh - 4.4rem)'
      m='0'
      bg='green.50'

    >
      {children}
    </Container>
  );
};

export default Layout;
