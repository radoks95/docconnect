/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import NavAuth from './NavAuth';
import * as ROUTES from '../../common/routes';
import {LOGIN, SIGN_UP} from '../../common/constants';


describe('NavAuth component', () => {
  it('renders the login and sign up links', () => {
    render(
        <BrowserRouter>
          <NavAuth />
        </BrowserRouter>,
    );

    const loginLink = screen.getByText(LOGIN);
    expect(loginLink).toBeInTheDocument();

    const signUpLink = screen.getByText(SIGN_UP);
    expect(signUpLink).toBeInTheDocument();
  });

  it('navigates to Login page when "Login" link is clicked', () => {
    render(
        <BrowserRouter>
          <NavAuth />
        </BrowserRouter>,
    );

    const loginLink = screen.getByText(LOGIN);
    fireEvent.click(loginLink);

    expect(window.location.pathname).toBe(ROUTES.LOGIN_PAGE);
  });

  it('navigates to Sign up page when "Sign Up" button is clicked', () => {
    render(
        <BrowserRouter>
          <NavAuth />
        </BrowserRouter>,
    );

    const signUpButton = screen.getByText(SIGN_UP).closest('button');
    fireEvent.click(signUpButton);

    expect(window.location.pathname).toBe(ROUTES.SIGN_UP_PAGE);
  });
});
