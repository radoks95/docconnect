import {Box, Button} from '@chakra-ui/react';
import {NavLink, useMatch} from 'react-router-dom';
import {LOGIN, SIGN_UP} from '../../common/constants';
import * as ROUTES from '../../common/routes';
import NavAuthTheme from '../../theme/NavAuthTheme';

const NavAuth = () => {
  const matchLogin = useMatch(ROUTES.LOGIN_PAGE);
  const matchSignUp = useMatch(ROUTES.SIGN_UP_PAGE);

  return (
    <Box __css={NavAuthTheme.baseStyle}>
      <NavLink to={ROUTES.LOGIN_PAGE}>
        <Box __css={matchLogin && NavAuthTheme.matchLogin} _hover={{transform: 'scale(1.12)'}}>
          {LOGIN}
        </Box>
      </NavLink>
      <NavLink to={ROUTES.SIGN_UP_PAGE}>
        <Button
          __css={!matchSignUp ? NavAuthTheme.buttonStyle : NavAuthTheme.matchSignUp}>
          {SIGN_UP}
        </Button>
      </NavLink>
    </Box>
  );
};

export default NavAuth;
