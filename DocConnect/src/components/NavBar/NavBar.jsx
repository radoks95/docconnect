import {Box, useBreakpointValue} from '@chakra-ui/react';
import Logo from '../Logo/Logo';
import NavButton from '../NavButton/NavButton';
import * as ROUTES from '../../common/routes';
import NavAuth from '../NavAuth/NavAuth';
import NavDrawer from '../NavDrawer/NavDrawer';
import {APPOINTMENTS, SPECIALISTS, SPECIALTIES} from '../../common/constants';
import NavBarTheme from '../../theme/NavBarTheme';
import {useSelector} from 'react-redux';
import NavAccountMenu from '../NavAccountMenu/NavAccountMenu';


const NavBar = () => {
  const user = useSelector((state) => state.user.user);
  const isMobile = useBreakpointValue({base: true, md: false}, {ssr: false});

  return (
    <Box __css={isMobile ? NavBarTheme.mobileStyle : NavBarTheme.baseStyle}>
      <Logo />
      {!isMobile ? (
        <>
          <Box __css={NavBarTheme.navButtons}>
            <NavButton route={ROUTES.HOME_PAGE}>
              {SPECIALTIES}
            </NavButton>
            <NavButton route={ROUTES.SPECIALISTS_PAGE}>
              {SPECIALISTS}
            </NavButton>
            {user.email && <NavButton route={ROUTES.APPOINTMENTS_PAGE}>
              {APPOINTMENTS}
            </NavButton>}
          </Box>
          {!user.email && <NavAuth />}
          {user.email && <NavAccountMenu user={user}/>}
        </>
      ) : (
        <NavDrawer />
      )}
    </Box>
  );
};

export default NavBar;
