/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import Logo from './Logo';
import {BrowserRouter} from 'react-router-dom';
import {HOME_PAGE} from '../../common/routes';


describe('Logo component', () => {
  it('renders the logo correctly and onClick navigate to Home page', () => {
    render(
        <BrowserRouter>
          <Logo />
        </BrowserRouter>,
    );

    const svgElement = screen.getByLabelText('DocConnect Logo');
    expect(svgElement).toBeInTheDocument();


    const headingElement = screen.getByRole('heading', {name: 'DocConnect'});
    expect(headingElement).toBeInTheDocument();

    // Simulate a click on the NavLink
    fireEvent.click(headingElement);

    // Check that the browser location matches HOME_PAGE
    expect(window.location.pathname).toBe(HOME_PAGE);
  });
});
