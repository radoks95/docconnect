import {Box, Heading} from '@chakra-ui/react';
import {NavLink} from 'react-router-dom';
import {HOME_PAGE} from '../../common/routes';

const Logo = () => {
  return (
    <NavLink to={HOME_PAGE}>
      <Box
        display='flex'
        justifyContent='space-between'
        alignItems='center'
        gap='0.4rem'
        maxH='2.2rem'
        _hover={{transform: 'scale(1.05)'}}
        aria-label='DocConnect Logo'>
        <svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M17.5 33.25C26.1985 33.25 33.25 26.1985
         33.25 17.5C33.25 8.80152 26.1985 1.75 17.5 1.75C8.80152 1.75 1.75 8.80152
          1.75 17.5C1.75 26.1985 8.80152 33.25 17.5 33.25ZM15.9252 33.1718V28.525C15.9252
           28.525 8.05017 26.95 8.05017
            19.8625V9.625H12.7752V8.05H15.9252V14.35H12.7752V12.775H11.2002V19.8625C11.2002 19.8625
             11.9877 25.375 17.5002
             25.375C23.0127
              25.375 23.8002
               19.8625 23.8002 19.8625V12.775H22.2252V14.35H19.0752V8.05H22.2252V9.625H26.9502V19.8625C26.9502
              26.95 19.0752 28.525 19.0752 28.525V33.1718C19.0752 33.1718
               18.1187 33.2499 17.4999 33.25C16.8813 33.25 15.9252 33.1718
               15.9252 33.1718Z" fill="#68D391"/>
        </svg>
        <Heading
          fontSize='24px'
          fontWeight='400'
          lineHeight='29px'
          letterSpacing='0em'
          textAlign='center'
          color='#FFF2F8'>
        DocConnect
        </Heading>
      </Box>
    </NavLink>
  );
};

export default Logo;
