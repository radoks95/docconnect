/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom'; // BrowserRouter for Link
import AuthMessage from './AuthMessage';


describe('AuthMessage component', () => {
  const mockProps = {
    image: 'image-url',
    alt: 'alt-text',
    heading: 'Test Heading',
    text: 'Test Text',
    linkTo: '/login',
    link: 'Test Link',
  };

  it('renders with the correct content', () => {
    render(
        <BrowserRouter>
          <AuthMessage {...mockProps} />
        </BrowserRouter>,
    );

    // Check for the presence of rendered elements and their content
    expect(screen.getByAltText('alt-text')).toBeInTheDocument();
    expect(screen.getByText('Test Heading')).toBeInTheDocument();
    expect(screen.getByText('Test Text')).toBeInTheDocument();
    expect(screen.getByText('Test Link')).toBeInTheDocument();
    fireEvent.click(screen.getByText('Test Link'));

    expect(window.location.pathname).toBe('/login');
  });
});

