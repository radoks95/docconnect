import {Box, Heading, Img, Text} from '@chakra-ui/react';
import {Link} from 'react-router-dom';
import AuthMessageTheme from '../../theme/AuthMessageTheme';

const AuthMessage = ({image, alt, heading, text, linkTo = '', link, clickHandler = () => {}}) => {
  return (
    <Box __css={AuthMessageTheme.baseStyle}>
      <Box __css={AuthMessageTheme.wrapperStyle}>
        <Img src={image} alt={alt} w='5rem' h='5rem' />
        <Heading
          textAlign='center'
          fontSize='24px'
          fontWeight='700'>
          {heading}
        </Heading>
        <Text
          alignSelf='stretch'
          fontSize='16px'
          fontWeight='400'>
          {text}
        </Text>
        <Box alignSelf='flex-start'>
          <Link to={linkTo}>
            <Text onClick={clickHandler} color='green.500'>
              {link}
            </Text>
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default AuthMessage;
