import {Box, Text, FormControl} from '@chakra-ui/react';
import {Select} from 'chakra-react-select';
import {useCities} from '../../hooks/useCities';

const CityInput = ({city, handleCityChange}) => {
  const {cities, isLoading, error} = useCities();

  const citiesOptions = cities.map((item) => ({
    value: item.name,
    label: item.name,
  }));

  return (
    <Box w='100%' maxW={{base: '250px', sm: '350px', md: '200px', lg: '300px', xl: '400px'}}>
      <Text fontSize="1rem" color="green.800" fontWeight="700" my="2px">
        City
      </Text>
      <FormControl>
        <Box bg='white' w='100%'>
          <Select
            isSearchable
            name="cities"
            options={(!error || !isLoading) ?
            citiesOptions : [{values: 'No options found', label: 'No options found'}]}
            placeholder=""
            value={{label: city}}
            onChange={handleCityChange}
            closeMenuOnSelect={true}
            focusBorderColor='green.500'
          />
        </Box>
      </FormControl>
    </Box>
  );
};

export default CityInput;
