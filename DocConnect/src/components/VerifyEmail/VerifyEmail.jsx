import AuthMessage from '../../components/AuthMessage/AuthMessage';
import success from '../../../public/success.svg';
import greenError from '../../../public/greenError.svg';
import {CONGRATULATIONS, LOGIN_LINK} from '../../common/constants';
import {LOGIN_PAGE} from '../../common/routes';
import LoadingSpiner from '../LoadingSpiner/LoadingSpiner';

const VerifyEmail = ({isInvalidToken, isAlreadyVerified, isLoadingTokenValidation, handleResendVerification}) => {
  const alreadyConfirmed = [isAlreadyVerified].includes('Email is already confirmed');

  if (isLoadingTokenValidation === false) return <LoadingSpiner />;

  return (
    <>
      {!isInvalidToken ? (
        <AuthMessage
          image={success}
          alt={'Success Icon'}
          heading={CONGRATULATIONS}
          text={'Your account was successfully verified.'}
          linkTo={LOGIN_PAGE}
          link={LOGIN_LINK}
        />

      ) : (
        <AuthMessage
          image={greenError}
          alt={'Error Icon'}
          heading={alreadyConfirmed ?
             ('Your account has already been verified!') :
             ('The token has expired or is invalid')}
          text={alreadyConfirmed ?
             ('You can login and use all of the website functionalities.') :
             ('Please request another verification email.')}
          linkTo={alreadyConfirmed && LOGIN_PAGE}
          link={alreadyConfirmed ? (LOGIN_LINK) : ('Request a new email')}
          clickHandler={handleResendVerification}
        />
      )}
    </>
  );
};

export default VerifyEmail;
