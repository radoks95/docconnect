/* eslint-disable no-undef */
import {render, screen} from '@testing-library/react';
import VerifyEmail from './VerifyEmail';
import {CONGRATULATIONS, LOGIN_LINK} from '../../common/constants';
import {LOGIN_PAGE} from '../../common/routes';
import {BrowserRouter} from 'react-router-dom';

describe('VerifyEmail component', () => {
  it('renders success message', () => {
    const {getByAltText, getByText} = render(
        <BrowserRouter>
          <VerifyEmail isInvalidToken={false} />
        </BrowserRouter>,
    );

    expect(getByAltText('Success Icon')).toBeInTheDocument();
    expect(getByText(CONGRATULATIONS)).toBeInTheDocument();
    expect(getByText('Your account was successfully verified.')).toBeInTheDocument();
    expect(screen.getByRole('link', {name: LOGIN_LINK})).toHaveAttribute('href', LOGIN_PAGE);
  });

  it('renders error message', () => {
    const {getByAltText, getByText} = render(
        <BrowserRouter>
          <VerifyEmail isInvalidToken={true} />
        </BrowserRouter>,
    );

    expect(getByAltText('Error Icon')).toBeInTheDocument();
    expect(getByText('The token has expired or is invalid')).toBeInTheDocument();
    expect(getByText('Please request another verification email.')).toBeInTheDocument();
    expect(screen.getByText('Request a new email')).toBeInTheDocument();
  });
});
