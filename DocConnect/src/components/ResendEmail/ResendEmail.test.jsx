/* eslint-disable no-undef */
import {render} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import ResendEmail from './ResendEmail';

describe('ResendEmail component', () => {
  it('renders success message', () => {
    const {getByAltText, getByText} = render(
        <BrowserRouter>
          <ResendEmail errorResend={null} />
        </BrowserRouter>,
    );

    expect(getByAltText('Success Icon')).toBeInTheDocument();
    expect(getByText('Success')).toBeInTheDocument();
    expect(getByText('A new email was sent to you to verify your email.')).toBeInTheDocument();
  });

  it('renders error message', () => {
    const errorMessage = 'An error occurred while resending the email.';
    const {getByAltText, getByText} = render(
        <BrowserRouter>
          <ResendEmail errorResend={errorMessage} />
        </BrowserRouter>,
    );

    expect(getByAltText('Error Icon')).toBeInTheDocument();
    expect(getByText('Error')).toBeInTheDocument();
    expect(getByText(errorMessage)).toBeInTheDocument();
  });
});
