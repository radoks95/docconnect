import AuthMessage from '../AuthMessage/AuthMessage';
import success from '../../../public/success.svg';
import greenError from '../../../public/greenError.svg';
import {LOGIN_LINK} from '../../common/constants';
import {LOGIN_PAGE} from '../../common/routes';

const ResendEmail = ({errorResend}) => {
  const alreadyConfirmed = [errorResend].includes('Email is already confirmed');

  return (
    !errorResend ? (
        <AuthMessage
          image={success}
          alt={'Success Icon'}
          heading={'Success'}
          text={'A new email was sent to you to verify your email.'} />
    ) : (
        <AuthMessage
          image={greenError}
          alt={'Error Icon'}
          heading={alreadyConfirmed ? ('Your account has already been verified!') : ('Error')}
          text={alreadyConfirmed ? ('You can login and use all of the website functionalities.') : (errorResend)}
          linkTo={alreadyConfirmed && LOGIN_PAGE}
          link={alreadyConfirmed && LOGIN_LINK} />
    )
  );
};

export default ResendEmail;
