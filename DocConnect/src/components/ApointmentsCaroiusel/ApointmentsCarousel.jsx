import {useState, useEffect} from 'react';
import {Box, Grid, IconButton, GridItem, useBreakpointValue} from '@chakra-ui/react';
import {ChevronLeftIcon, ChevronRightIcon} from '@chakra-ui/icons';
import AppointmentColumn from '../AppointmentColumn/AppointmentColumn';
import {Swiper, SwiperSlide} from 'swiper/react';
import {Navigation} from 'swiper/modules';
import 'swiper/css';
import './styles.css';
import {getNextDays} from '../../common/helpers';
import {useDoctorAppointments} from '../../hooks/useDoctorAppointments';
import ServerErrorMessage from '../ServerErrorMessage/ServerErrorMessage';


const AppointmentsCarousel = ({id, setAppointmentSlot}) => {
  const {appointments, isLoading, error} = useDoctorAppointments(id);
  const [isActiveButton, setIsActiveButton] = useState('');


  const columnCount = useBreakpointValue({base: 1, sm: 1, md: 2, lg: 3});

  const nextDays = getNextDays(30);

  const [currentDate, setCurrentDate] = useState(nextDays[0]);

  const totalDays = 30;

  const renderGridItems = (pageIndex) => {
    const startIndex = pageIndex * columnCount;
    const endIndex = Math.min((pageIndex + 1) * columnCount, totalDays);

    const dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    return Array.from({length: endIndex - startIndex}, (_, dayIndex) => {
      const day = nextDays[startIndex + dayIndex].getDate();
      const date = nextDays[startIndex + dayIndex];
      const dayOfWeek = dayNames[date.getDay()];
      return (
        <GridItem key={dayIndex}>
          <AppointmentColumn
            day={dayOfWeek}
            date={`${date.getMonth() + 1}-${day}-${date.getFullYear()}`}
            appointments={appointments}
            setAppointmentSlot={setAppointmentSlot}
            isActiveButton={isActiveButton}
            setIsActiveButton={setIsActiveButton} />
        </GridItem>
      );
    });
  };

  const pages = Array.from({length: Math.ceil(totalDays / columnCount)}, (_, pageIndex) => pageIndex);

  useEffect(() => {
    if (currentDate.getDate() === 1) {
      setCurrentDate(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1));
    }
  }, [currentDate]);

  const handlePrevClick = () => {
    const newDate = new Date(currentDate);
    newDate.setDate(newDate.getDate() - totalDays);
    setCurrentDate(newDate);
  };

  const handleNextClick = () => {
    const newDate = new Date(currentDate);
    newDate.setDate(newDate.getDate() + totalDays);
    setCurrentDate(newDate);
  };

  if (isLoading) return;

  if (error) return <Box display='flex' justifyContent='center'><ServerErrorMessage text={error.message} /></Box>;


  return (
    <Box display='flex' w='100%' justifyContent='space-evenly'>
      <IconButton icon={<ChevronLeftIcon />}
        className='custom-prev'
        color='white'
        bgColor='green.500'
        _hover={{bgColor: 'green.700'}}
        onClick={handlePrevClick} />
      <Swiper
        modules={[Navigation]}
        navigation={{
          prevEl: '.custom-prev',
          nextEl: '.custom-next',
        }}
        className='mySwiper'
      >
        {pages.map((pageIndex) => (
          <SwiperSlide key={pageIndex}>
            <Grid templateColumns={`repeat(${columnCount}, 1fr)`} gap='20px'>
              {renderGridItems(pageIndex)}
            </Grid>
          </SwiperSlide>
        ))}
      </Swiper>
      <IconButton icon={<ChevronRightIcon />}
        className='custom-next'
        color='white'
        bgColor='green.500'
        _hover={{bgColor: 'green.700'}}
        onClick={handleNextClick} />
    </Box>
  );
};

export default AppointmentsCarousel;
