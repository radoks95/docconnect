import AuthMessage from '../../components/AuthMessage/AuthMessage';
import attention from '../../../public/attention.svg';
import {FORGOT_PASSWORD} from '../../common/constants';
import greenError from '../../../public/greenError.svg';


const SendEmailForResetPassword = ({error, email}) => {
  return (
    !error ? (
        <AuthMessage
          image={attention}
          alt={'Exclamation mark'}
          heading={FORGOT_PASSWORD}
          text={
            <>
              If a matching account was found an email was sent to{' '}
              <span style={{fontWeight: 'bold'}}>
                {email}
              </span>{' '}
              to allow you to reset your password.
            </>
          }
        />
    ) : (
        <AuthMessage
          image={greenError}
          alt={'Error Icon'}
          heading={'Unsuccessful request'}
          text={error} />
    )
  );
};

export default SendEmailForResetPassword;
