import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Text,
  useBreakpointValue,
} from '@chakra-ui/react';
import ForgotPasswordTheme from '../../theme/ForgotPasswordTheme';

const ForgotPassword = ({formik}) => {
  const isMobile = useBreakpointValue({base: true, md: false}, {ssr: false});


  return (
    <Box __css={!isMobile ? ForgotPasswordTheme.baseStyle : ForgotPasswordTheme.baseStyleMobile}>
      <Box alignSelf='stretch' gap="5px" >
        <Heading fontSize='1.5rem'>Forgot Password</Heading>
        <Box __css={!isMobile ? ForgotPasswordTheme.boxStyle : ForgotPasswordTheme.boxStyleMobile} mt='0.5rem'>
          <Text fontSize='14px' w='100%'>
            Please enter your email address. You will receive a link to create a new password via email.
          </Text>
        </Box>
      </Box>

      <form onSubmit={formik.handleSubmit}>
        <Box gap="20px">

          <FormControl isInvalid={formik.touched.email && formik.errors.email}>
            <Box __css={!isMobile ? ForgotPasswordTheme.boxStyle : ForgotPasswordTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='email'>
                  Email Address *
              </FormLabel >
              <Input
                h='2.4rem'
                placeholder='placeholder@email.com'
                id='email'
                name='email'
                type='email'
                focusBorderColor='green.500'
                value={formik.values.email}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}/>
              {(formik.touched.email && formik.errors.email) &&
                (<FormErrorMessage color='red.600'>{formik.errors.email}</FormErrorMessage>)}
            </Box>
          </FormControl>
          <Button __css={ForgotPasswordTheme.buttonStyle} type='submit'>
                Submit
          </Button>


        </Box>
      </form>

    </Box>
  );
};

export default ForgotPassword;
