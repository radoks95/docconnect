/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import {ChakraProvider} from '@chakra-ui/react';
import ForgotPassword from './ForgotPassword';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import store from '../../store/store';

// Mock matchMedia
global.matchMedia = global.matchMedia || function() {
  return {
    addListener: vi.fn(),
    removeListener: vi.fn(),
  };
};

describe('ForgotPassword component', () => {
  it('should submit the form with valid data', () => {
    const formikMock = {
      values: {
        email: 'test@example.com',
      },
      touched: {
        email: true,
        password: true,
      },
      errors: {},
      handleBlur: vi.fn(),
      handleChange: vi.fn(),
      handleSubmit: vi.fn(),
    };

    render(
        <ChakraProvider>
          <Provider store={store}>
            <BrowserRouter>
              <ForgotPassword formik={formikMock} />
            </BrowserRouter>
          </Provider>
        </ChakraProvider>,
    );


    const submitButton = screen.getByRole('button', {name: 'Submit'});

    fireEvent.click(submitButton);

    expect(formikMock.handleSubmit).toHaveBeenCalled();
  });
});

it('should display error messages for invalid fields', () => {
  const formikMock = {
    values: {
      email: '',
    },
    touched: {
      email: true,
    },
    errors: {
      email: 'Please enter your email address.',
    },
    handleBlur: vi.fn(),
    handleChange: vi.fn(),
    handleSubmit: vi.fn(),
  };

  render(
      <ChakraProvider>
        <Provider store={store}>
          <BrowserRouter>
            <ForgotPassword formik={formikMock} />
          </BrowserRouter>
        </Provider>
      </ChakraProvider>,
  );

  const submitButton = screen.getByRole('button', {name: 'Submit'});

  fireEvent.click(submitButton);

  expect(formikMock.handleSubmit).toHaveBeenCalled();

  expect(screen.getByText('Please enter your email address.')).toBeInTheDocument();
});
