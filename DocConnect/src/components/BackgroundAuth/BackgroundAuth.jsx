import {Box} from '@chakra-ui/react';
import './BackgroundAuth.css';


const BackgroundAuth = () => {
  return (
    <Box className="container-fluid">
      <Box className="ocean">
        <Box className="wave"></Box>
        <Box className="wave"></Box>
        <Box className="wave"></Box>
      </Box>
    </Box>
  );
};

export default BackgroundAuth;
