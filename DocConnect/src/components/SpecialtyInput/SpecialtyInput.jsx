import {Box, Text, FormControl} from '@chakra-ui/react';

import {Select} from 'chakra-react-select';
import {useSpecialties} from '../../hooks/useSpecialties';

const SpecialtyInput = ({specialty, handleSpecialtyChange}) => {
  const {specialties, isLoading, error} = useSpecialties();

  const specialtiesOptions = specialties.map((item) => ({
    value: item.name,
    label: item.name,
  }));
  return (
    <Box w='100%' maxW={{base: '250px', sm: '350px', md: '200px', lg: '300px', xl: '400px'}}>
      <Text fontSize="1rem" color="green.800" fontWeight="700" my="2px">
        Specialty
      </Text>
      <FormControl>
        <Box bg='white'>
          <Select
            isSearchable
            name="specialties"
            options={
              !error || !isLoading ?
                specialtiesOptions :
                [{value: 'no options found', label: 'No options found'}]
            }
            placeholder=""
            value={{label: specialty}}
            closeMenuOnSelect={true}
            onChange={handleSpecialtyChange}
            focusBorderColor='green.500' />
        </Box>
      </FormControl>
    </Box>
  );
};

export default SpecialtyInput;
