import {Box, Heading, Text} from '@chakra-ui/react';
import AppointmentColumnItem from '../AppointmentColumnItem/AppointmentColumnItem';


const AppointmentColumn = ({day, date, appointments, setAppointmentSlot, isActiveButton, setIsActiveButton}) => {
  const workHours = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'];
  const weekEnds = day === 'Saturday' || day === 'Sunday';
  return (
    <Box
      w='100%'
      display='flex'
      flexDirection='column'
      px='10px'
      gap='16px'
      alignItems='center'
    >
      <Box textAlign='center'>
        <Heading fontSize='16px' fontWeight='700' color='green.900'>
          {day}
        </Heading>
        <Text fontSize='16px' fontWeight='700' color='green.900'>
          {date}
        </Text>
      </Box>
      {workHours.map((item) => (
        !weekEnds ? (
          <AppointmentColumnItem
            key={item}
            date={date}
            hour={item}
            appointments={appointments}
            setAppointmentSlot={setAppointmentSlot}
            isActiveButton={isActiveButton}
            setIsActiveButton={setIsActiveButton} />
        ) : (
          <AppointmentColumnItem key={item} hour={'Day off'} />
        )
      ))}
    </Box>
  );
};

export default AppointmentColumn;
