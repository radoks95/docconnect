import {Text} from '@chakra-ui/react';
import {SUPPORT_EMAIL} from '../../common/constants';

const TermsAndConditionsText = () => {
  return (
    <>
      <Text>Last Updated: September 7, 2023</Text>
      <br />
      <Text>
        Welcome to our lively and learning-centric doctor appointment booking
        website! We're excited to have you on board for some informative and
        entertaining experiences. Before we get started, let's get the legal
        dance moves down. By using our website, you're agreeing to some terms
        and conditions. Don't worry, we'll keep it simple and even throw in a
        few jokes along the way!
      </Text>
      <br />
      <Text>
        1. Learning is Our Jam
        <br />
        This website is all about learning, experimenting, and having a good
        time. It's like the science fair of doctor appointment booking
        platforms. We're not real doctors, just code magicians in the world of
        healthcare.
      </Text>
      <br />
      <Text>
        2. Age Matters
        <br />
        Here's the scoop: you need to be 18 years old or above to hang out here.
        It's like a cool kids' club, but without the secret handshake. Sorry,
        younger pals – this virtual playground is for the grown-ups.
      </Text>
      <br />
      <Text>
        3. Be Respectful
        <br />
        Respect is the key ingredient to our digital potluck. Be nice to each
        other, share knowledge, and remember that we're all here to learn and
        have a chuckle. No trolling or spamming allowed – we're more about
        unicorns than internet trolls.
      </Text>
      <br />
      <Text>
        4. Intellectual Property
        <br />
        Tap Dance Everything on this website is like our unique dance moves –
        copyrighted! Don't copy, reproduce, or use our stuff without our
        permission. Imagine it's a fancy dance move – you can't just steal it
        and show it off as your own.
      </Text>
      <br />
      <Text>
        5. Links, Not Leaps of Faith
        <br />
        We might share links to external sites, but they're like suggested dance
        partners, not blind leaps of faith. We're not responsible for what
        happens once you leave our dance floor, so shimmy with caution.
      </Text>
      <br />
      <Text>
        6. Don't Break the Code
        <br />
        Please don't try to break our website or do anything funky with the
        code. It's like sticking your foot out to trip someone on the dance
        floor – it's just not cool.
      </Text>
      <br />
      <Text>
        7. Privacy Polka
        <br />
        We're all about privacy – your info is our VIP guest. Check out our
        privacy policy for more details on how we treat your data. Just
        remember, no one likes an uninvited party crasher, especially when it's
        your personal info.
      </Text>
      <br />
      <Text>
        8. Changes, the Cha-Cha Kind
        <br />
        Like a dance routine, our terms might change. We'll keep you posted, so
        keep an eye out for updates. By sticking around, you're saying you're
        cool with the changes.
      </Text>
      <br />
      <Text>
        9. Have Fun and Learn!
        <br />
        We're here to make learning entertaining and healthcare topics a little
        less intimidating. So, have fun, stay curious, and remember, no real
        doctors here – just a bunch of friendly AI and pixels.
      </Text>
      <br />
      <Text>
        Thanks for joining us on this dance floor of knowledge and learning! If
        you've got questions or want to bust out some moves, reach out to us at {SUPPORT_EMAIL}.
        <br />
        <br />
        Happy dancing and exploring!
        <br />
        <br />
        Sincerely, The Groovy Guide Gnome
      </Text>
    </>
  );
};

export default TermsAndConditionsText;
