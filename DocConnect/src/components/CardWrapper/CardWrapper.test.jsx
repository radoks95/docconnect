/* eslint-disable no-undef */
import {render, screen} from '@testing-library/react';
import CardWrapper from './CardWrapper';

describe('CardWrapper component', () => {
  it('renders the correct children components', () => {
    const children = [
      <div key={1}>Child 1</div>,
      <div key={2}>Child 2</div>,
      <div key={3}>Child 3</div>,
    ];

    render(<CardWrapper>{children}</CardWrapper>);

    const childElements = screen.getAllByText(/Child/);

    expect(childElements.length).toBe(children.length);

    // Assure that each child element is rendered
    childElements.forEach((childElement, index) => {
      expect(childElement.textContent).toBe(`Child ${index + 1}`);
    });
  });
});
