import {Box, Grid} from '@chakra-ui/react';

const CardWrapper = ({children}) => {
  return (
    <Box>
      <Grid templateColumns={{
        base: 'repeat(1, 1fr)',
        sm: 'repeat(1, 1fr)',
        md: 'repeat(2, 1fr)',
        lg: 'repeat(2, 1fr)',
        xl: 'repeat(3, 1fr)',
      }}
      gap={{
        base: '2rem',
        sm: '2rem',
        md: '2rem',
        lg: '4rem',
      }}
      gridAutoFlow="dense"
      justifyContent="center" >
        {children}
      </Grid>
    </Box>
  );
};

export default CardWrapper;
