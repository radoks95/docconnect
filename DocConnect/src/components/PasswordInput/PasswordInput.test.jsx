/* eslint-disable no-undef */
import {render, fireEvent} from '@testing-library/react';
import PasswordInput from './PasswordInput';

describe('PasswordInput component functionality', () => {
  it('toggles password visibility when the button is clicked', () => {
    render(
        <PasswordInput id="password" value="" onChange={() => {}} />,
    );

    // Get the input element and the toggle button
    const input = document.querySelector('input');
    const toggleButton = document.querySelector('button');

    // Initial state: password type should be 'password'
    expect(input.type).toBe('password');

    // Click the toggle button
    fireEvent.click(toggleButton);

    // After clicking: password type should be 'text'
    expect(input.type).toBe('text');

    // Click the toggle button again
    fireEvent.click(toggleButton);

    // After clicking again: password type should be 'password'
    expect(input.type).toBe('password');
  });
});
