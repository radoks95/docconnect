import {Input, InputGroup, InputRightElement, Button, Tooltip} from '@chakra-ui/react';
import {ViewIcon, ViewOffIcon} from '@chakra-ui/icons';
import {useState} from 'react';


const PasswordInput = ({id, value, onBlur = () => {}, onChange}) => {
  const [show, setShow] = useState(false);
  const handleClick = () => setShow(!show);

  return (
    <InputGroup size='md'>
      <Input
        pr='4.5rem'
        h='2.4rem'
        focusBorderColor='green.500'
        placeholder='Enter password'
        id={id}
        name={id}
        type={show ? 'text' : 'password'}
        value={value}
        onBlur={onBlur}
        onChange={onChange}
      />
      <InputRightElement width='4.5rem'>
        <Button h='1.75rem' size='sm' onClick={handleClick} bg='white'>
          {show ? (
          <Tooltip label='Hide password'>
            <ViewOffIcon h='100%' w='100%' />
          </Tooltip>
            ) : (
            <Tooltip label='Show password'>
              <ViewIcon h='100%' w='100%'/>
            </Tooltip>)}
        </Button>
      </InputRightElement>
    </InputGroup>
  );
};

export default PasswordInput;
