import {Heading} from '@chakra-ui/react';

const PageTitle = ({children}) => {
  return (
    <Heading
      as="h1"
      textAlign="left"
      fontSize="36px"
      fontStyle="normal"
      fontWeight={700}
      lineHeight="normal"
      mb='2.2rem'
    >
      {children}
    </Heading>
  );
};

export default PageTitle;
