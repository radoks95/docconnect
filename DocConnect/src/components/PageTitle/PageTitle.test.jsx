import {render, screen} from '@testing-library/react';
import {describe, test, expect} from 'vitest';
import PageTitle from './PageTitle';

describe('page title', () => {
  test('should appear in screen', () => {
    render(<PageTitle>Hello test</PageTitle>);
    expect(screen.getByText('Hello test')).toBeDefined();
  });
});
