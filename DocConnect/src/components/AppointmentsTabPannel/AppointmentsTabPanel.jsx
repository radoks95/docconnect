import {ChevronRightIcon, ChevronLeftIcon} from '@chakra-ui/icons';
import {TabPanel,
  Accordion,
  Box,
  Button,
  Heading,
} from '@chakra-ui/react';
import {useState} from 'react';
import AppointmentsTableItem from '../AppointmentsTableItem/AppointmentsTableItem';
import {useBreakpointValue} from '@chakra-ui/react';
import {getAppointmentDate} from '../../common/helpers';
import {usePagination} from '../../hooks/usePagination';


const AppointmentsTabPanel = ({appointments, hideCancelButton}) => {
  const [isAccordionItemOpen, setIsAccordionItemOpen] = useState([-1]);
  const isMobile = useBreakpointValue({base: true, sm: false});
  const {
    handleNextPage,
    handlePrevPage,
    handlePageClick,
    slicedData,
    pageNumbers,
    currentPage,
    pageCount,
  } = usePagination(appointments);

  const accordionChangeHandler = (expandedIndex) => {
    setIsAccordionItemOpen(expandedIndex);
  };

  if (slicedData.length === 0 && currentPage !== 0) {
    handlePrevPage();
  }

  if (slicedData.length === 0) {
    return (
      <TabPanel pb='44px' h='57.3vh' display='flex' justifyContent='center' mt='5rem'>
        <Heading textAlign='center'>You don't have appointments yet.</Heading>
      </TabPanel>
    );
  }

  return (
    <TabPanel pb='44px'>
      <Accordion allowToggle mb="40px" minH="50vh" index={isAccordionItemOpen} onChange={accordionChangeHandler}>
        {slicedData.map((item) => {
          const {formattedDate, formattedTime} = getAppointmentDate(item.timeSlot);

          return (
            <AppointmentsTableItem
              key={item.id}
              id={item.id}
              specialtyName={item.specialityName}
              date={formattedDate}
              hour={formattedTime}
              doctor={item.doctorFullName}
              location={item.location}
              appointments={appointments}
              hideCancelButton={hideCancelButton}
              setIsAccordionItemOpen={setIsAccordionItemOpen}
            />
          );
        })}
      </Accordion>

      <Box display="flex" alignItems="center" justifyContent="center">
        {!isMobile && (
          <Button
            onClick={handlePrevPage}
            disabled={currentPage === 0}
            variant="outline"
            border='none'
            color='green.600'
            mr="1em"
          >
            <ChevronLeftIcon boxSize={6} />
          </Button>
        )}

        <Box display="flex" gap='3px'>
          {pageNumbers.map((pageNumber) => (
            <Button
              key={pageNumber}
              onClick={() => handlePageClick(pageNumber)}
              variant={pageNumber === currentPage ? 'solid' : 'outline'}
              borderRadius='100'
              color={pageNumber === currentPage ? 'white' : 'green.200'}
              bg={pageNumber === currentPage ? 'green.200' : 'transparent'}
            >
              {pageNumber + 1}
            </Button>
          ))}
        </Box>

        {!isMobile && (
          <Button
            onClick={handleNextPage}
            disabled={currentPage === pageCount - 1 || pageCount <= 1}
            variant="outline"
            border='none'
            color='green.600'
            ml="1em"
          >
            <ChevronRightIcon boxSize={6} />
          </Button>
        )}
      </Box>
    </TabPanel>
  );
};

export default AppointmentsTabPanel;
