import {Button} from '@chakra-ui/react';
import {calculateReservedDays, formatDate} from '../../common/helpers';


const AppointmentColumnItem = ({date, hour, appointments, setAppointmentSlot, isActiveButton, setIsActiveButton}) => {
  const isDayOf = hour === 'Day off';
  const formattedHour = hour === '09:00' ? '9:00' : hour;
  const reservedDays = calculateReservedDays(appointments?.appointments);
  const isReserved = reservedDays.includes(`${date}T${formattedHour}`);

  const appointmentHandler = (e) => {
    const timeSlot = formatDate(e.target.value);
    const [date, hour] = timeSlot.split('T');
    const formattedHour = Number(hour.substring(0, 2));
    setAppointmentSlot({date, formattedHour});
    setIsActiveButton(timeSlot);
  };


  return (
    <Button
      w='100%'
      padding="10px 54px"
      border-radius="5px"
      border={isDayOf ? 'none' : '3px solid #38A169'}
      color={(isDayOf || isActiveButton === formatDate(`${date} ${hour}`)) ? 'white' : 'green.900'}
      bg={isDayOf ?
      'gray.900' : (isReserved || isActiveButton === formatDate(`${date} ${hour}`)) ? 'green.500' : 'white'}
      isDisabled={isDayOf || isReserved}
      _hover={(!isDayOf && !isReserved) && {bg: 'green.400', color: 'white'}}
      value={`${date} ${hour}`}
      onClick={(e) => appointmentHandler(e)}>
      {hour}
    </Button>
  );
};

export default AppointmentColumnItem;
