import {Text} from '@chakra-ui/react';
import {SUPPORT_EMAIL} from '../../common/constants';

const PrivacyPolicyText = () => {
  return (
    <>
      <Text>Last Updated: September 7, 2023</Text>
      <br />
      <Text>
        Welcome to our learning-oriented doctor appointment booking website!
        Before you proceed, let's get comfy with some serious yet lighthearted
        privacy matters. Remember, while we're here to have a bit of fun, we
        take your privacy seriously. So, grab your cup of virtual tea and let's
        dive in!
      </Text>
      <br />
      <Text>
        1. Your Data is Our Secret Sauce
        <br />
        Here's the deal: we don't share your personal data with anyone. We're
        like that best friend who keeps your secrets – safely hidden from prying
        eyes. Your name, contact details, and any other info you provide are
        only used for the purpose of doctor appointment bookings. No secret
        ingredient sharing here!
        <br />
        <br />
        2. Age is Just a Number
        <br />
        Guess what? We need you to be 18 years old or above to use our platform.
        No offense to our younger pals, but some medical stuff requires a bit
        more grown-up wisdom. So, if you're not old enough to buy lottery
        tickets, this isn't the appointment booking carnival for you.
        <br />
        <br />
        3. Cookies, Not the Edible Kind
        <br />
        Like most websites, we use cookies. No, they won't steal your cookies
        from the pantry, but they help us understand how you use our site. It's
        like peeking into our kitchen to see which recipes you love most. Rest
        assured, these digital cookies don't munch on your data.
        <br />
        <br />
        4. We're Not Mind Readers
        <br />
        We might know you need a doctor, but we're not psychic. So, if you share
        your symptoms and health info with us, we promise not to use our
        newfound mind-reading powers. Your health data stays locked away, and we
        only use it to help you find the right doctor.
        <br />
        <br />
        5. Data Security Dance
        <br />
        Picture this: we're doing the security cha-cha to protect your data. Our
        servers have their own bouncers, and we've hired digital bodyguards to
        keep your info safe. We're as serious about security as a squirrel
        guarding its nut stash.
        <br />
        <br />
        6. Third-Party Tango
        <br />
        Occasionally, we may dance with third-party services like analytics
        tools. Don't worry, it's a well-choreographed routine, and your personal
        info remains backstage. Just remember, we're the star of your data
        protection show.
        <br />
        <br />
        7. Your Rights, Your Stage
        <br />
        You're the lead in this show! If you want to know what info we've got on
        you, just give us a shout. We'll even let you change or delete it if
        you're not feeling the rhythm anymore.
        <br />
        <br />
        8. Updates, the Non-Boring Kind Our privacy policy might change, but
        we'll let you know. No surprises, just like your favorite TV show's plot
        twists. So, check in now and then to stay in the loop.
        <br />
        <br />
        9. Have a Laugh with Us
        <br />
        We're all about learning, having a good time, and making awkward health
        topics a little less cringeworthy. But when it comes to your privacy,
        we're as serious as a cat that accidentally turned on the vacuum
        cleaner.
        <br />
        <br />
      </Text>
      <Text>
        Remember, this website is a learning playground, so no real doctors here
        – just friendly AI and a bunch of code. If you need actual medical
        advice, consult a real, live, breathing human doc.
      </Text>
      <br />
      <Text>
        Thanks for choosing us to be part of your online learning journey!
      </Text>
      <br />
      <Text>
        If you've got privacy questions or just want to say hi, drop us a line
        at {SUPPORT_EMAIL}.
      </Text>
      <br />
      <Text>Happy browsing and learning!</Text>
      <br />
      <Text>Sincerely, The Privacy Protector Penguins</Text>
    </>
  );
};

export default PrivacyPolicyText;
