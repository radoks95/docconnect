import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  FormErrorMessage,
  FormLabel,
  Heading,
  useBreakpointValue,
} from '@chakra-ui/react';
import ResetPasswordTheme from '../../theme/ResetPasswordTheme';
import PasswordInput from '../PasswordInput/PasswordInput';

const ResetPassword = ({formik}) => {
  const isMobile = useBreakpointValue({base: true, md: false}, {ssr: false});

  return (
    <Box __css={!isMobile ? ResetPasswordTheme.baseStyle : ResetPasswordTheme.baseStyleMobile}>
      <Box alignSelf='stretch' gap="5px" >
        <Heading fontSize='1.5rem'>Reset Password</Heading>
      </Box>

      <form onSubmit={formik.handleSubmit}>
        <Box gap="20px">

          <FormControl isInvalid={formik.errors.newPassword && formik.touched.newPassword}>
            <Box __css={!isMobile ? ResetPasswordTheme.boxStyle : ResetPasswordTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='password'>
                Password *
              </FormLabel>
              <PasswordInput
                id={'newPassword'}
                value={formik.values.newPassword}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange} />
              {formik.touched.newPassword && formik.errors.newPassword ? (
                  <FormErrorMessage color='red.600'>{formik.errors.newPassword}</FormErrorMessage>
                ) : (
                  <FormHelperText p='5px' color='gray.500' fontSize='0.8rem'>
                 Use 8 or more characters, with a mix of uppercase,
                 lowercase, numbers and symbols.
                  </FormHelperText>
                )}
            </Box>
          </FormControl>


          <FormControl isInvalid={formik.touched.confirmPassword && formik.errors.confirmPassword}>
            <Box __css={!isMobile ? ResetPasswordTheme.boxStyle : ResetPasswordTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='confirmPassword'>
                Confirm Password *
              </FormLabel>
              <PasswordInput
                id={'confirmPassword'}
                value={formik.values.confirmPassword}
                onChange={formik.handleChange} />
              {(formik.touched.confirmPassword && formik.errors.confirmPassword) &&
                  (<FormErrorMessage color='red.600'>{formik.errors.confirmPassword}</FormErrorMessage>)}
            </Box>
          </FormControl>

          <Button __css={ResetPasswordTheme.buttonStyle} type='submit'>
                Reset
          </Button>

        </Box>
      </form>

    </Box>
  );
};

export default ResetPassword;

