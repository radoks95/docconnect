/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import {ChakraProvider} from '@chakra-ui/react';
import ResetPassword from './ResetPassword';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import store from '../../store/store';

// Mock matchMedia
global.matchMedia = global.matchMedia || function() {
  return {
    addListener: vi.fn(),
    removeListener: vi.fn(),
  };
};

describe('ResetPassword component', () => {
  it('should submit the form with valid data', () => {
    const formikMock = {
      values: {
        password: 'P@ssw0rd',
        confirmPassword: 'P@ssw0rd',
      },
      touched: {
        password: true,
        confirmPassword: true,
      },
      errors: {},
      handleBlur: vi.fn(),
      handleChange: vi.fn(),
      handleSubmit: vi.fn(),
    };

    render(
        <ChakraProvider>
          <Provider store={store}>
            <BrowserRouter>
              <ResetPassword formik={formikMock} />
            </BrowserRouter>
          </Provider>
        </ChakraProvider>,
    );


    const submitButton = screen.getByRole('button', {name: 'Reset'});

    fireEvent.click(submitButton);

    expect(formikMock.handleSubmit).toHaveBeenCalled();
  });
});

it('should display error messages for invalid fields', () => {
  const formikMock = {
    values: {
      newPassword: '',
      confirmPassword: '',
    },
    touched: {
      newPassword: true,
      confirmPassword: true,
    },
    errors: {
      newPassword: 'Password is required',
      confirmPassword: 'Passwords do not match',
    },
    handleBlur: vi.fn(),
    handleChange: vi.fn(),
    handleSubmit: vi.fn(),
  };

  render(
      <ChakraProvider>
        <Provider store={store}>
          <BrowserRouter>
            <ResetPassword formik={formikMock} />
          </BrowserRouter>
        </Provider>
      </ChakraProvider>,
  );

  const submitButton = screen.getByRole('button', {name: 'Reset'});

  fireEvent.click(submitButton);

  expect(formikMock.handleSubmit).toHaveBeenCalled();

  expect(screen.getByText('Password is required')).toBeInTheDocument();
  expect(screen.getByText('Passwords do not match')).toBeInTheDocument();
});
