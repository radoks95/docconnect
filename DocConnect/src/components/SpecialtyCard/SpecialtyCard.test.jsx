/* eslint-disable no-undef */
import {render, screen} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import {ChakraProvider} from '@chakra-ui/react';
import SpecialtyCard from './SpecialtyCard';


const specialty = {
  name: 'Cardiology',
  imageUrl:
   'https://images.pexels.com/photos/7659564/pexels-photo-7659564.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
};

describe('SpecialtyCard component', () => {
  it('renders the specialty name and image correctly', () => {
    render(
        <ChakraProvider>
          <BrowserRouter>
            <SpecialtyCard specialty={specialty} />
          </BrowserRouter>
        </ChakraProvider>,
    );

    const specialtyName = screen.getByText(specialty.name);
    expect(specialtyName).toBeInTheDocument();

    const specialtyImage = screen.getByRole('img');
    expect(specialtyImage).toBeInTheDocument();
    expect(specialtyImage).toHaveAttribute('src', specialty.imageURL);
    expect(specialtyImage).toHaveAttribute('alt', specialty.name);
  });

  it('links to the correct specialty page', () => {
    render(
        <ChakraProvider>
          <BrowserRouter>
            <SpecialtyCard specialty={specialty} />
          </BrowserRouter>
        </ChakraProvider>,
    );

    // NavLink links to the correct URL
    const specialtyLink = screen.getByRole('link');
    expect(specialtyLink).toHaveAttribute('href', `/specialists?specialistName=&specialty=${specialty.name}`);
  });
});
