import {Box, Image, Heading} from '@chakra-ui/react';
import {NavLink} from 'react-router-dom';
import SpecialtyCardTheme from '../../theme/SpecialtyCardTheme';

const SpecialtyCard = ({specialty: {name, imageUrl}}) => {
  return (
    <NavLink to={`/specialists?specialistName=&specialty=${name}&city=`}>
      <Box
        __css={SpecialtyCardTheme.baseStyle}>
        <Box position="relative" display="inline-block">
          <Image src={imageUrl} alt={name} w="25rem" h="21rem" objectFit="cover" objectPosition=" left center"/>
          <Box __css={SpecialtyCardTheme.imageBox}/>
        </Box>
        <Box __css={SpecialtyCardTheme.headingBox}>
          <Heading fontSize="24px">
            {name}
          </Heading>
        </Box>
      </Box>
    </NavLink>
  );
};

export default SpecialtyCard;
