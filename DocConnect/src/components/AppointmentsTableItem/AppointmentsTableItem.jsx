import {
  AccordionItem,
  AccordionButton,
  Box,
  Heading,
  Text,
  Img,
  AccordionIcon,
  AccordionPanel,
} from '@chakra-ui/react';
import locationSvg from '../../../public/location.svg';
import {useBreakpointValue} from '@chakra-ui/react';
import {deleteAppointmentById} from '../../services/appointments.service';
import {useShowToast} from '../../hooks/useShowToast';
import {useDispatch} from 'react-redux';
import {upcomingAppointmentsActions} from '../../slices/upcomingAppointmentSlice';
import AppointmentConfirmationModal from '../AppointmentConfirmationModal/AppointmentConfirmationModal';

const AppointmentsTableItem = ({
  date, doctor, location, hour, appointments, setIsAccordionItemOpen, hideCancelButton, id, specialtyName,
}) => {
  const dispatch = useDispatch();
  const isMobile = useBreakpointValue({base: true, sm: false});
  const showToast = useShowToast();


  const deleteAppointmentHandler = async (id) => {
    try {
      await deleteAppointmentById(id);
      dispatch(upcomingAppointmentsActions.setUpcomingAppointments(appointments
          .filter((appointment) => appointment.id !== Number(id))));
      setIsAccordionItemOpen(-1);
      showToast('Success', 'You have successfully canceled this appointment', 'success');
    } catch (error) {
      showToast('Error', error.message, 'error');
    }
  };

  return (
    <AccordionItem border='1px' m='0.5rem' borderColor='gray.200'>
      <h2>
        <AccordionButton id={id}>
          <Box display='flex' justifyContent='space-between' gap='34px'>
            <Box as="span" textAlign='left' >
              <Heading fontSize='16px'>Date</Heading>
              <Text>{date}</Text>
              <Text>{hour}</Text>
            </Box>
            <Box as="span" flex='1' textAlign='left'>
              <Heading fontSize='16px'>Doctor</Heading>
              <Text>{doctor}</Text>
              <Text color='green.600'>{specialtyName}</Text>
            </Box>
          </Box>
          {isMobile ? null : (
            <Box as="span" flex='1' textAlign='right'>
              Details
            </Box>
          )}
          <AccordionIcon />
        </AccordionButton>
      </h2>
      <AccordionPanel pb={4}>
        <Box display='flex' justifyContent='space-between' gap='34px'>
          <Box textAlign='left'>
            <Heading fontSize='16px' mb='6px'>Address</Heading>
            <Box display='flex' flexDirection='row' gap='10px'>
              <Img src={locationSvg} />
              <Text>{location}</Text>
            </Box>
          </Box>
          {!hideCancelButton &&
          <AppointmentConfirmationModal appointmentId={id} deleteAppointmentHandler={deleteAppointmentHandler} /> }
        </Box>
      </AccordionPanel>
    </AccordionItem>
  );
};

export default AppointmentsTableItem;
