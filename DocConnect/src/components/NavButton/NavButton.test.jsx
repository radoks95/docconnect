/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import NavButton from './NavButton';

const BUTTON_TEXT = 'NavButton Test';
const ROUTE = '/test';

describe('NavButton component', () => {
  it('renders the button with default color and no underline when not matched', () => {
    render(
        <BrowserRouter>
          <NavButton route={ROUTE}>{BUTTON_TEXT}</NavButton>
        </BrowserRouter>,
    );

    const button = screen.getByText(BUTTON_TEXT).closest('a');
    expect(button).toBeInTheDocument();
  });

  it('renders the button with custom color and underline when matched', () => {
    render(
        <BrowserRouter initialEntries={[ROUTE]}>
          <NavButton route={ROUTE} buttonColor="#ABCDEF">
            {BUTTON_TEXT}
          </NavButton>
        </BrowserRouter>,
    );

    const button = screen.getByText(BUTTON_TEXT).closest('a');
    expect(button).toBeInTheDocument();
  });

  it('navigates to the specified route when button is clicked', () => {
    render(
        <BrowserRouter>
          <NavButton route={ROUTE}>{BUTTON_TEXT}</NavButton>
        </BrowserRouter>,
    );

    const button = screen.getByText(BUTTON_TEXT).closest('a');
    fireEvent.click(button);

    expect(window.location.pathname).toBe(ROUTE);
  });
});
