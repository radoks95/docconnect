import {Box} from '@chakra-ui/react';
import {NavLink} from 'react-router-dom';
import {useMatch} from 'react-router-dom';

const NavButton = ({children, route, buttonColor = '#FFF2F8', onClick}) => {
  const match = useMatch(`/${route}`);
  return (
    <NavLink
      to={route}>
      <Box
        color={match ? 'green.200' : buttonColor}
        textDecoration={match ? 'underline' : 'none'}
        _hover={{transform: 'scale(1.1)'}}
        onClick={onClick}>
        {children}
      </Box>
    </NavLink>
  );
};

export default NavButton;
