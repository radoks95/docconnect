import {
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Icon,
  useDisclosure} from '@chakra-ui/react';
import {useRef} from 'react';
import {APPOINTMENTS, LOGIN, SIGN_UP, SPECIALISTS, SPECIALTIES} from '../../common/constants';
import * as ROUTES from '../../common/routes';
import NavButton from '../NavButton/NavButton';
import {HamburgerIcon} from '@chakra-ui/icons';
import NavDrawerTheme from '../../theme/NavDrawerTheme';
import {useSelector, useDispatch} from 'react-redux';
import {handleLogout} from '../../common/helpers';
import {useNavigate} from 'react-router';


const NavDrawer = () => {
  const user = useSelector((state) => state.user.user);
  const {isOpen, onOpen, onClose} = useDisclosure();
  const btnRef = useRef();
  const dispatch = useDispatch();
  const navigate = useNavigate();


  return (
    <>
      <Button ref={btnRef} colorScheme='white' onClick={onOpen}>
        <Icon as={HamburgerIcon} w='1.2rem' h='1.2rem' />
      </Button>
      <Drawer
        isOpen={isOpen}
        placement='right'
        onClose={onClose}
        finalFocusRef={btnRef}>
        <DrawerOverlay />
        <DrawerContent mt='4rem'>
          <DrawerCloseButton />
          <DrawerHeader></DrawerHeader>
          <DrawerBody>
            <Box __css={NavDrawerTheme.baseStyle} onClick={onClose}>
              <NavButton
                route={ROUTES.HOME_PAGE}
                buttonColor=''>
                {SPECIALTIES}
              </NavButton>
              <NavButton
                route={ROUTES.SPECIALISTS_PAGE}
                buttonColor=''>
                {SPECIALISTS}
              </NavButton>
              {user.email && <NavButton
                route={ROUTES.APPOINTMENTS_PAGE}
                buttonColor=''>
                {APPOINTMENTS}
              </NavButton>}
              {!user.email && (<>
                <NavButton
                  route={ROUTES.LOGIN_PAGE}
                  buttonColor=''>
                  {LOGIN}
                </NavButton>
                <NavButton
                  route={ROUTES.SIGN_UP_PAGE}
                  buttonColor=''>
                  {SIGN_UP}
                </NavButton>
              </>)
              }
              {user.email && (<>
                <NavButton
                  route={ROUTES.LOGIN_PAGE}
                  buttonColor=''
                  onClick={()=>{
                    handleLogout(dispatch), navigate(ROUTES.HOME_PAGE);
                  }}>
                  Logout
                </NavButton>
              </>)
              }
            </Box>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
};

export default NavDrawer;
