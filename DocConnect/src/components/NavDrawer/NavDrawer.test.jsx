/* eslint-disable no-undef */
import {render, screen} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import NavDrawer from './NavDrawer';
import * as ROUTES from '../../common/routes';
import {Provider} from 'react-redux';
import store from '../../store/store';

const getByNavButtonRoute = (route) => {
  return screen.getByRole('button', {name: route});
};

describe('NavDrawer component', () => {
  it('renders NavButtons with correct props', () => {
    render(
        <BrowserRouter>
          <Provider store={store}>
            <NavDrawer />
          </Provider>
        </BrowserRouter>,
    );

    // Find the NavButton components with correct route prop
    const specialtiesButton = getByNavButtonRoute(ROUTES.SPECIALTIES);
    const specialistsButton = getByNavButtonRoute(ROUTES.SPECIALISTS);
    const loginButton = getByNavButtonRoute(ROUTES.LOGIN);
    const signUpButton = getByNavButtonRoute(ROUTES.SIGN_UP);

    expect(specialtiesButton).toBeInTheDocument();
    expect(specialistsButton).toBeInTheDocument();
    expect(loginButton).toBeInTheDocument();
    expect(signUpButton).toBeInTheDocument();
  });
});

