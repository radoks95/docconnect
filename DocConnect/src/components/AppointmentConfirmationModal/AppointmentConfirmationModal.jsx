import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from '@chakra-ui/react';

const AppointmentConfirmationModal = ({appointmentId, deleteAppointmentHandler}) => {
  const {isOpen, onOpen, onClose} = useDisclosure();

  return (
    <>
      <Button onClick={onOpen} bg='green.600' color='white' _hover={{bg: 'green.700'}}>Cancel</Button>

      <Modal blockScrollOnMount={false} isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Cancel Appointment</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text fontWeight='bold' mb='1rem'>
                Are you sure you want to cancel this appointment?
            </Text>
          </ModalBody>

          <ModalFooter>
            <Button bg='gray.300' mr={3} onClick={onClose}>
                Close
            </Button>
            <Button bg='green.600'
              color='white'
              _hover={{bg: 'green.700'}}
              onClick={() => deleteAppointmentHandler(appointmentId)}>
                Cancel Appointment
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AppointmentConfirmationModal;
