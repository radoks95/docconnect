import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Text,
  Box,
  Checkbox,
} from '@chakra-ui/react';
import {useRef, useState} from 'react';

const CompanyPolicyModal = ({children, title, text, value, id, formik}) => {
  const {isOpen, onOpen, onClose} = useDisclosure();

  const btnRef = useRef(null);
  const modalBodyRef = useRef(null);
  const [showDoneButton, setShowDoneButton] = useState(false);

  const handleScroll = () => {
    if (modalBodyRef.current) {
      const {scrollTop, scrollHeight, clientHeight} = modalBodyRef.current;
      const isAtEnd = scrollTop + clientHeight >= scrollHeight - 16;
      setShowDoneButton(isAtEnd);
    }
  };

  return (
    <>
      <Box display='flex' flexDirection='row' gap='0.5rem' alignItems='flex-start'>
        <Checkbox
          mt='0.2rem'
          isDisabled
          isChecked={value}
          value={value}
          id={id}/>
        <Text fontSize='15px'>
          {text}
          <Text as='span'
            textDecoration='underline'
            cursor='pointer'
            fontSize='15px'
            color={value && 'green.500'}
            _hover={{color: value ? 'black' : 'green.500'}}
            ref={btnRef}
            onClick={onOpen}>
            {title}
          </Text>
          {' '+'*'}
        </Text>

      </Box>

      <Modal
        onClose={() => {
          onClose(), setShowDoneButton(false);
        }}
        finalFocusRef={btnRef}
        isOpen={isOpen}
        scrollBehavior='inside'
      >
        <ModalOverlay />
        <ModalContent my='6rem' mx='1rem' maxH='80%'>
          <ModalHeader>{title}</ModalHeader>
          <ModalCloseButton />
          <ModalBody ref={modalBodyRef} onScroll={handleScroll}>
            {children}
          </ModalBody>
          <ModalFooter h='16rem'>
            {showDoneButton && (
              <Box
                display='flex'
                flexDirection='row'
                gap='0.5rem'
                mx='auto'
                alignItems='flex-start'>
                <Checkbox
                  mt='0.3rem'
                  colorScheme='green'
                  id={id}
                  isChecked={value}
                  value={value}
                  onChange={formik.handleChange} />
                <Text fontSize='18px'>
                  {`${text} ${title}`}
                </Text>
              </Box>
            )}
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CompanyPolicyModal;
