/* eslint-disable no-undef */
import {render, screen, fireEvent} from '@testing-library/react';
import {ChakraProvider} from '@chakra-ui/react';
import SignUp from './SignUp';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import store from '../../store/store';

// Mock matchMedia
global.matchMedia = global.matchMedia || function() {
  return {
    addListener: vi.fn(),
    removeListener: vi.fn(),
  };
};

describe('SignUp component', () => {
  it('should submit the form with valid data', () => {
    const formikMock = {
      values: {
        email: 'test@example.com',
        firstName: 'John',
        lastName: 'Doe',
        password: 'P@ssw0rd',
        confirmPassword: 'P@ssw0rd',
      },
      touched: {
        email: true,
        firstName: true,
        lastName: true,
        password: true,
        confirmPassword: true,
      },
      errors: {},
      handleBlur: vi.fn(),
      handleChange: vi.fn(),
      handleSubmit: vi.fn(),
    };

    render(
        <ChakraProvider>
          <Provider store={store}>
            <BrowserRouter>
              <SignUp formik={formikMock} />
            </BrowserRouter>
          </Provider>
        </ChakraProvider>,
    );


    const submitButton = screen.getByRole('button', {name: 'Sign up'});

    fireEvent.click(submitButton);

    expect(formikMock.handleSubmit).toHaveBeenCalled();
  });
});

it('should display error messages for invalid fields', () => {
  const formikMock = {
    values: {
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      confirmPassword: '',
    },
    touched: {
      email: true,
      firstName: true,
      lastName: true,
      password: true,
      confirmPassword: true,
    },
    errors: {
      email: 'Email is required',
      firstName: 'First name is required',
      lastName: 'Last name is required',
      password: 'Password is required',
      confirmPassword: 'Passwords do not match',
    },
    handleBlur: vi.fn(),
    handleChange: vi.fn(),
    handleSubmit: vi.fn(),
  };

  render(
      <ChakraProvider>
        <Provider store={store}>
          <BrowserRouter>
            <SignUp formik={formikMock} />
          </BrowserRouter>
        </Provider>
      </ChakraProvider>,
  );

  const submitButton = screen.getByRole('button', {name: 'Sign up'});

  fireEvent.click(submitButton);

  expect(formikMock.handleSubmit).toHaveBeenCalled();

  expect(screen.getByText('Email is required')).toBeInTheDocument();
  expect(screen.getByText('First name is required')).toBeInTheDocument();
  expect(screen.getByText('Last name is required')).toBeInTheDocument();
  expect(screen.getByText('Password is required')).toBeInTheDocument();
  expect(screen.getByText('Passwords do not match')).toBeInTheDocument();
});
