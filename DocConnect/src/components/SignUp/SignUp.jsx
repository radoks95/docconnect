import {
  Box,
  Heading,
  Text,
  Input,
  Button,
  FormControl,
  FormLabel,
  useBreakpointValue,
  FormErrorMessage,
  FormHelperText,
  Checkbox,
  Tooltip}
  from '@chakra-ui/react';
import {Link} from 'react-router-dom';
import PasswordInput from '../../components/PasswordInput/PasswordInput';
import {LOGIN_PAGE} from '../../common/routes';
import SignUpTheme from '../../theme/SignUpTheme';
import CompanyPolicyModal from '../CompanyPolicyModal/CompanyPolicyModal';
import PrivacyPolicyText from '../PrivacyPolicyText/PrivacyPolicyText';
import {
  PRIVACY_POLICY,
  PRIVACY_POLICY_TEXT,
  TERMS_AND_CONDITIONS,
  TERMS_AND_CONDITIONS_TEXT,
} from '../../common/constants';
import TermsAndConditionsText from '../TermsAndConditionsText/TermsAndConditionsText';


const SignUp = ({formik}) => {
  const isMobile = useBreakpointValue({base: true, md: false}, {ssr: false});

  return (
    <Box __css={!isMobile ? SignUpTheme.baseStyle : SignUpTheme.baseStyleMobile}>
      <Box alignSelf='stretch' gap="5px" >
        <Heading fontSize='1.5rem'>Sign up</Heading>
        <Box display='flex' gap='5px'flexDirection='row'>
          <Text fontSize='1rem'>Already have an account?</Text>
          <Link to={LOGIN_PAGE}><Text color='green.500' fontSize='1rem' textDecoration='underline'>Login</Text></Link>
        </Box>
      </Box>

      <form onSubmit={formik.handleSubmit}>
        <Box gap="20px">

          <FormControl isInvalid={formik.touched.email && formik.errors.email}>
            <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='email'>
                  Email Address *
              </FormLabel >
              <Input
                h='2.4rem'
                placeholder='placeholder@email.com'
                id='email'
                name='email'
                type='email'
                focusBorderColor='green.500'
                value={formik.values.email}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}/>
              {(formik.touched.email && formik.errors.email) &&
                (<FormErrorMessage color='red.600'>{formik.errors.email}</FormErrorMessage>)}
            </Box>
          </FormControl>

          <FormControl isInvalid={formik.touched.firstName && formik.errors.firstName}>
            <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='firstName'>
                  First Name *
              </FormLabel>
              <Input
                h='2.4rem'
                placeholder='First Name'
                id='firstName'
                name='firstName'
                type='text'
                focusBorderColor='green.500'
                value={formik.values.firstName}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}/>
              {(formik.touched.firstName && formik.errors.firstName) &&
                (<FormErrorMessage color='red.600'>{formik.errors.firstName}</FormErrorMessage>)}
            </Box>
          </FormControl>

          <FormControl isInvalid={formik.touched.lastName && formik.errors.lastName}>
            <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='lastName'>
                  Last Name *
              </FormLabel>
              <Input
                h='2.4rem'
                placeholder='Last Name'
                id='lastName'
                name='lastName'
                type='text'
                focusBorderColor='green.500'
                value={formik.values.lastName}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}/>
              {(formik.touched.lastName && formik.errors.lastName) &&
              (<FormErrorMessage color='red.600'>{formik.errors.lastName}</FormErrorMessage>)}
            </Box>
          </FormControl>

          <FormControl isInvalid={formik.errors.password && formik.touched.password}>
            <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='password'>
                Password *
              </FormLabel>
              <PasswordInput
                id={'password'}
                value={formik.values.password}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange} />
              {formik.touched.password && formik.errors.password ? (
                  <FormErrorMessage color='red.600'>{formik.errors.password}</FormErrorMessage>
                ) : (
                  <FormHelperText p='5px' color='gray.500' fontSize='0.8rem'>
                 Use 8 or more characters, with a mix of uppercase,
                 lowercase, numbers and symbols.
                  </FormHelperText>
                )}
            </Box>
          </FormControl>

          <FormControl isInvalid={formik.touched.confirmPassword && formik.errors.confirmPassword}>
            <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile}>
              <FormLabel fontSize='14px' htmlFor='confirmPassword'>
                Confirm Password *
              </FormLabel>
              <PasswordInput
                id={'confirmPassword'}
                value={formik.values.confirmPassword}
                onChange={formik.handleChange} />
              {(formik.touched.confirmPassword && formik.errors.confirmPassword) &&
                  (<FormErrorMessage color='red.600'>{formik.errors.confirmPassword}</FormErrorMessage>)}
            </Box>
          </FormControl>

          <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile} >
            <Box display='flex' flexDirection='row' gap='0.5rem' alignItems='flex-start'>
              <Checkbox
                mt='0.2rem'
                colorScheme='green'
                value={formik.values.confirmAge}
                onChange={formik.handleChange}
                id='confirmAge'/>
              <Tooltip label='
This ensures you can make informed decisions about your health. Thank you for understanding!'>
                <Text fontSize='15px'>
            I confirm that I am at least 18 years old *
                </Text>
              </Tooltip>
            </Box>
            {(formik.touched.confirmAge && formik.errors.confirmAge) &&
                  (<Text mb='1rem' color='red.600' fontSize='14px'>{formik.errors.confirmAge}</Text>)}
          </Box>

          <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile}>
            <CompanyPolicyModal
              title={PRIVACY_POLICY}
              text={PRIVACY_POLICY_TEXT}
              value={formik.values.confirmPolicy}
              formik={formik}
              id={'confirmPolicy'}>
              <PrivacyPolicyText/>
            </CompanyPolicyModal>
            {(formik.touched.confirmPolicy && formik.errors.confirmPolicy) &&
                    (<Text mb='1rem' color='red.600' fontSize='14px'>{formik.errors.confirmPolicy}</Text>)}
          </Box>

          <Box __css={!isMobile ? SignUpTheme.boxStyle : SignUpTheme.boxStyleMobile}>
            <CompanyPolicyModal
              title={TERMS_AND_CONDITIONS}
              text={TERMS_AND_CONDITIONS_TEXT}
              value={formik.values.confirmTerms}
              formik={formik}
              id={'confirmTerms'}>
              <TermsAndConditionsText />
            </CompanyPolicyModal>
            {(formik.touched.confirmTerms && formik.errors.confirmTerms) &&
                    (<Text mb='1rem' color='red.600' fontSize='14px'>{formik.errors.confirmTerms}</Text>)}
          </Box>

          <Button __css={SignUpTheme.buttonStyle} type='submit'>
                Sign up
          </Button>


        </Box>
      </form>

    </Box>
  );
};

export default SignUp;

