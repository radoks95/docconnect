import {Box, Text, InputGroup, InputRightElement, Input, Tooltip} from '@chakra-ui/react';
import {Search2Icon} from '@chakra-ui/icons';


const SpecialistInput = ({specialistName, handleSpecialistNameChange}) => {
  return (
    <Box w='100%' maxW={{base: '250px', sm: '350px', md: '200px', lg: '300px', xl: '400px'}}>
      <Text fontSize="1rem" color='green.800' fontWeight='700' my='2px'>Specialist Name</Text>
      <InputGroup>
        <Input
          h="2.6rem"
          bg='white'
          id="specialistName"
          name="specialistName"
          type="text"
          value={specialistName}
          onChange={handleSpecialistNameChange}
          focusBorderColor='green.500'
        />
        <InputRightElement>
          <Tooltip label='Search specialist name'>
            <Search2Icon/>
          </Tooltip>
        </InputRightElement>
      </InputGroup>
    </Box>
  );
};

export default SpecialistInput;
