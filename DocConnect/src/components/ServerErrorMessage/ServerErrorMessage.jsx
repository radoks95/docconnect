import {Box, Img, Text} from '@chakra-ui/react';
import error from '../../../public/error.svg';

const ServerErrorMessage = ({text, width ='90%', maxWidth = '30rem'}) => {
  return (
    <Box
      display='flex'
      zIndex='5'
      mt='0.5rem'
      padding='1.4rem 2.1rem'
      alignItems='center'
      justifyContent='center'
      gap='0.7rem'
      w={width}
      maxW={maxWidth}
      borderRadius='0.3rem'
      background='var(--maximum-red, #D71C21)'
      boxShadow='0px 4px 4px 0px rgba(0, 0, 0, 0.5)'>
      <Img src={error} alt='error' />
      <Text
        textAlign='center'
        color='white'>
        {text}
      </Text>
    </Box>
  );
};

export default ServerErrorMessage;
