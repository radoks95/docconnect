/* eslint-disable no-undef */
import {render} from '@testing-library/react';
import ServerErrorMessage from './ServerErrorMessage';

describe('ServerErrorMessage component functionality', () => {
  it('renders the error text and image correctly', () => {
    const errorMessage = 'An error occurred. Please try again.';

    const {getByText, getByAltText} = render(
        <ServerErrorMessage text={errorMessage} />,
    );

    const errorText = getByText(errorMessage);
    expect(errorText).toBeInTheDocument();

    const errorImage = getByAltText('error');
    expect(errorImage).toBeInTheDocument();
    expect(errorImage.src).toContain('/error.svg');
  });
});
