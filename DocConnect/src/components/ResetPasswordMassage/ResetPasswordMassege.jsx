
import {CONGRATULATIONS, CONGRATULATIONS_RESET_MSG, LOGIN_LINK} from '../../common/constants';
import {LOGIN_PAGE} from '../../common/routes';
import greenError from '../../../public/greenError.svg';
import success from '../../../public/success.svg';
import AuthMessage from '../../components/AuthMessage/AuthMessage';

const ResetPasswordMassege = ({error}) => {
  return (!error ? (

 <AuthMessage
   image={success}
   alt={'Success Icon'}
   heading={CONGRATULATIONS}
   text={CONGRATULATIONS_RESET_MSG}
   linkTo={LOGIN_PAGE}
   link={LOGIN_LINK} />

  ): (
    <AuthMessage
      image={greenError}
      alt={'Error Icon'}
      heading={'Unsuccessful request'}
      text={error} />
)

  );
};

export default ResetPasswordMassege;
