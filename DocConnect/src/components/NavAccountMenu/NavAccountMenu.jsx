import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Avatar,
} from '@chakra-ui/react';
import {useDispatch} from 'react-redux';
import * as ROUTES from '../../common/routes';
import {handleLogout} from '../../common/helpers';
import {useNavigate} from 'react-router';


const NavAccountMenu = (user) => {
  const dispatch = useDispatch();
  const firstName = user.user.firstName;
  const lastName = user.user.lastName;
  const userName = `${firstName} ${lastName}`;


  const navigate = useNavigate();

  return (
    <Menu>
      <MenuButton _hover={{transform: 'scale(1.2)'}}>
        <Avatar name={userName} size='sm' src='https://bit.ly/broken-link' />
      </MenuButton>
      <MenuList>
        <MenuItem onClick={()=>{
          handleLogout(dispatch), navigate(ROUTES.HOME_PAGE);
        }}>Log out</MenuItem>
      </MenuList>
    </Menu>
  );
};

export default NavAccountMenu;
