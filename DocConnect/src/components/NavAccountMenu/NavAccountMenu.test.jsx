/* eslint-disable no-undef */
import {render, fireEvent} from '@testing-library/react';
import NavAccountMenu from './NavAccountMenu';
import {useDispatch} from 'react-redux';
import * as helperLogout from '../../common/helpers';
import {BrowserRouter} from 'react-router-dom';

vi.mock('react-redux', () => ({
  useDispatch: vi.fn(),
}));

describe('NavAccountMenu', () => {
  const mockDispatch = vi.fn();
  const mockHandleLogout = vi.spyOn(helperLogout, 'handleLogout');

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  it('dispatches handleLogout when Log out is clicked', () => {
    const {getByText} = render(
        <BrowserRouter>
          <NavAccountMenu user={{fullName: ['John', 'Doe']}} />
        </BrowserRouter>);
    const logoutButton = getByText('Log out');

    fireEvent.click(logoutButton);

    expect(mockHandleLogout).toHaveBeenCalledTimes(1);
  });
});
