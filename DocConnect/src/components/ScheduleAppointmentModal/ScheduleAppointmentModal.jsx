import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Tooltip,
  useDisclosure,
} from '@chakra-ui/react';
import SpecialistCardTheme from '../../theme/SpecialistCardTheme';
import {useSelector} from 'react-redux';
import {useLocation, useNavigate} from 'react-router-dom';
import {LOGIN_PAGE} from '../../common/routes';
import ApointmentsCarousel from '../ApointmentsCaroiusel/ApointmentsCarousel';
import {useState} from 'react';
import {createAppointment} from '../../services/appointments.service';
import {useShowToast} from '../../hooks/useShowToast';
import {SUCCESSFULLY_MADE_APPOINTMENT} from '../../common/constants';


const ScheduleAppointmentModal = ({id}) => {
  const {isOpen, onOpen, onClose} = useDisclosure();
  const showToast = useShowToast();
  const [appointmentSlot, setAppointmentSlot] = useState('');
  const user = useSelector((state) => state.user.user);
  const navigate = useNavigate();
  const location = useLocation();

  const appointmentHandler = (e) => {
    e.preventDefault();
    if (!user.id) {
      navigate(LOGIN_PAGE);
      showToast('', 'You need to log in to continue', 'error');
      return;
    }
    onOpen();
  };

  const scheduleAppointmentHandler = async (e) => {
    e.preventDefault();
    const appointmentDetails = {
      doctorId: id,
      userId: user.id,
      timeSlot: appointmentSlot.date,
      hour: appointmentSlot.formattedHour,
    };
    try {
      await createAppointment(appointmentDetails);
      closeModalHandler();
      showToast('Success', SUCCESSFULLY_MADE_APPOINTMENT, 'success');
    } catch (error) {
      showToast('Error', error.message, 'error');
    }
  };

  const closeModalHandler = () => {
    onClose();
    setAppointmentSlot('');
  };

  return (
    <>
      <Tooltip label={!user.id && 'You need to log in in order to schedule an appointment'}>
        <Button __css={SpecialistCardTheme.buttonStyle}
          maxW={location.pathname.includes('profile') && '17.3rem'}
          onClick={appointmentHandler}>Schedule an Appointment</Button>
      </Tooltip>

      <Modal isOpen={isOpen} onClose={closeModalHandler}>
        <ModalOverlay />
        <ModalContent
          display='flex'
          flexDirection='column'
          gap='2.5rem'
          mx='auto'
          my='6rem'
          p='2rem 2.5rem'
          minW={{
            sm: '20em',
            md: '40em',
            lg: '48em',
          }}
          h='auto'
          border='2px solid rgba(0, 0, 0, 0.15)'
          borderRadius='1rem'
          boxShadow='2px 2px 6px 0px rgba(0, 0, 0, 0.25)'
        >
          <ModalHeader fontSize='36px'>Schedule an Appointment</ModalHeader>
          <ModalBody >
            <ApointmentsCarousel id={id} setAppointmentSlot={setAppointmentSlot} />
          </ModalBody>

          <ModalFooter
            display='flex'
            justifyContent='flex-end'
            alignItems='flex-start'
            gap='0.6rem'
            alignSelf='stretch'>
            <Button
              backgroundColor='green.500'
              color='white'
              w='30%'
              _hover={{backgroundColor: 'green.600'}}
              onClick={closeModalHandler}>
                Cancel
            </Button>
            <Button
              backgroundColor='green.700'
              color='white'
              w='30%'
              _hover={{backgroundColor: 'green.900'}}
              isDisabled={!appointmentSlot}
              onClick={scheduleAppointmentHandler}>
                Schedule</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default ScheduleAppointmentModal;
