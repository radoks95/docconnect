import * as Yup from 'yup';
import {MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH} from '../common/constants';
import {PASSWORD_REG_EXPRESSION} from '../common/helpers';
export const resetPasswordValidationSchema = Yup.object({
  newPassword: Yup
      .string()
      .required('Please enter a password.')
      .matches(PASSWORD_REG_EXPRESSION,
          `Your password must have at least 8 characters, with a mix of uppercase, lowercase, numbers and symbols.`)
      .min(MIN_PASSWORD_LENGTH, `Password must be at least ${MIN_PASSWORD_LENGTH} characters long.`)
      .max(MAX_PASSWORD_LENGTH, `Password must be less than ${MAX_PASSWORD_LENGTH} characters long.`),
  confirmPassword: Yup
      .string()
      .required('Please confirm your password')
      .oneOf([Yup.ref('newPassword'), null], 'Those passwords didn’t match. Please try again.'),
});
