import * as Yup from 'yup';
import {MAX_NAME_LENGTH, MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH} from '../common/constants';
import {EMAIL_REG_EXPRESSION, PASSWORD_REG_EXPRESSION} from '../common/helpers';
export const signUpValidationSchema = Yup.object({
  email: Yup
      .string()
      .required('Please enter an email address.')
      .email('Please enter a valid email address.')
      .matches(EMAIL_REG_EXPRESSION, 'Please enter a valid email address.'),
  firstName: Yup
      .string()
      .required('Please enter a first name.')
      .max(MAX_NAME_LENGTH, `First name must be less than ${MAX_NAME_LENGTH} characters long.`),
  lastName: Yup
      .string()
      .required('Please enter a last name.')
      .max(MAX_NAME_LENGTH, `Last name must be less than ${MAX_NAME_LENGTH} characters long.`),
  password: Yup
      .string()
      .required('Please enter a password.')
      .matches(PASSWORD_REG_EXPRESSION,
          `Your password must have at least 8 characters, with a mix of uppercase, lowercase, numbers and symbols.`)
      .min(MIN_PASSWORD_LENGTH, `Password must be at least ${MIN_PASSWORD_LENGTH} characters long.`)
      .max(MAX_PASSWORD_LENGTH, `Password must be less than ${MAX_PASSWORD_LENGTH} characters long.`),
  confirmPassword: Yup
      .string()
      .required('Please confirm your password.')
      .oneOf([Yup.ref('password'), null], 'Those passwords didn’t match. Please try again.'),
  confirmAge: Yup
      .boolean()
      .oneOf([true], `Unfortunately, we can only offer our services to individuals who are 18 years old or above.
       This is because certain medical procedures and decisions require legal consent, and your safety and understanding
        are our top priorities. Thank you for your understanding and we hope to assist you in the future!`),
  confirmPolicy: Yup
      .boolean()
      .oneOf([true], `Please read and agree to the Privacy Policy in order to proceed.`),
  confirmTerms: Yup
      .boolean()
      .oneOf([true], `Please read and agree to the Terms and Conditions in order to proceed.`),
});
