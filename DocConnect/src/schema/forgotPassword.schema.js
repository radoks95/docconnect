import * as Yup from 'yup';
import {EMAIL_REG_EXPRESSION} from '../common/helpers';
export const forgotPasswordValidationSchema = Yup.object({
  email: Yup
      .string()
      .required('Please enter your email address.')
      .email('Please enter a valid email address.')
      .matches(EMAIL_REG_EXPRESSION, 'Please enter a valid email address.'),
});
