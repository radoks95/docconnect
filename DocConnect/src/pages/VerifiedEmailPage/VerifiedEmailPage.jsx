import BackgroundAuth from '../../components/BackgroundAuth/BackgroundAuth';
import {DOCUMENT_TITLE} from '../../common/constants';
import {useTitle} from '../../hooks/useTitle';
import {useParams} from 'react-router-dom';
import {useEffect, useState} from 'react';
import {resendEmailVerification, verifyUserAfterSignUp} from '../../services/auth.service';
import VerifyEmail from '../../components/VerifyEmail/VerifyEmail';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';
import ResendEmail from '../../components/ResendEmail/ResendEmail';

const VerifiedEmailPage = () => {
  const {email, token} = useParams();
  const [isInvalidToken, setIsInvalidToken] = useState(false);
  const [isLoadingTokenValidation, setIsLoadingTokenValidation] = useState(false);
  const [isAlreadyVerified, setIsAlreadyVerified] = useState(null);
  const [isRequestedResend, setIsRequestedResend] = useState(false);
  const [isLoadingResend, setIsLoadingResend] = useState(false);
  const [errorResend, setErrorResend] = useState(null);

  useEffect(() => {
    verifyUserAfterSignUp(email, token)
        .catch((error) => {
          setIsInvalidToken(true);
          setIsAlreadyVerified(error);
        })
        .finally(() => {
          setIsLoadingTokenValidation(true);
        });
  }, [email, token]);

  useTitle(DOCUMENT_TITLE.VERIFIED_EMAIL_PAGE);

  const handleResendVerification = async () => {
    try {
      setIsLoadingResend(true);
      await resendEmailVerification(email);
      setIsRequestedResend(true);
    } catch (error) {
      setIsRequestedResend(true);
      setErrorResend(error.message);
    } finally {
      setIsLoadingResend(false);
    }
  };

  if (isLoadingResend) return <><BackgroundAuth /><LoadingSpiner /></>;
  return (
    <>
      <BackgroundAuth />
      {!isRequestedResend ? (
        <VerifyEmail
          isInvalidToken={isInvalidToken}
          isAlreadyVerified={isAlreadyVerified}
          isLoadingTokenValidation={isLoadingTokenValidation}
          handleResendVerification={handleResendVerification} />
      ) : (
        <ResendEmail errorResend={errorResend} />
      )}
    </>
  );
};

export default VerifiedEmailPage;
