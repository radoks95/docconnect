import {Box} from '@chakra-ui/react';
import {APPOINTMENTS, DOCUMENT_TITLE} from '../../common/constants';
import {useTitle} from '../../hooks/useTitle';
import PageTitle from '../../components/PageTitle/PageTitle';
import AppointmentsTable from '../../components/AppoitmentsTable/AppointmentsTable';

const AppointmentsPage = () => {
  useTitle(DOCUMENT_TITLE.APPOINTMENTS_PAGE);

  return (
    <>
      <Box m='4.25rem 3rem'>
        <PageTitle>{APPOINTMENTS}</PageTitle>
        <AppointmentsTable/>
      </Box>
    </>

  );
};

export default AppointmentsPage;
