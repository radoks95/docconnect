import {useTitle} from '../../hooks/useTitle';
import {DOCUMENT_TITLE, SPECIALISTS} from '../../common/constants';
import {useSpecialists} from '../../hooks/useSpecialists';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';
import ServerErrorMessage from '../../components/ServerErrorMessage/ServerErrorMessage';
import SpecialistCard from '../../components/SpecialistCard/SpecialistCard';
import CardWrapper from '../../components/CardWrapper/CardWrapper';
import {GridItem, Box, useBreakpointValue, Heading, Tooltip} from '@chakra-ui/react';
import PageTitle from '../../components/PageTitle/PageTitle';
import SpecialistInput from '../../components/SpecialistInput/SpecialistInput';
import SpecialtyInput from '../../components/SpecialtyInput/SpecialtyInput';
import CityInput from '../../components/CityInput/CityInput';
import {useFilterSpecialists} from '../../hooks/useFilterSpecialists';
import {useState} from 'react';
import {useInfiniteScroll} from '../../hooks/useInfiniteScroll';
import {Icon} from '@chakra-ui/icons';
import {MdFilterAltOff} from 'react-icons/md';
import SpecialistsPageTheme from '../../theme/SpecialistsPageTheme';
import ScrollToTop from '../../components/ScrollToTop/ScrollToTop';


const SpecialistsPage = () => {
  const [page, setPage] = useState(1);
  const {specialists, isLoading, error} = useSpecialists(page, setPage);
  const {
    specialistName,
    handleSpecialistNameChange,
    specialty,
    handleSpecialtyChange,
    city,
    handleCityChange,
    onClearFiltersHandler,
  } = useFilterSpecialists(page);
  const title = specialty ? `${DOCUMENT_TITLE.SPECIALISTS_PAGE} ${specialty}` : DOCUMENT_TITLE.SPECIALISTS_PAGE;

  const isMobile = useBreakpointValue({base: true, md: false}, {ssr: false});

  useInfiniteScroll(setPage);

  useTitle(title);


  if (error) return <ServerErrorMessage text={error} />;


  return (
    <>
      <Box
        m='4.25rem 3rem'>
        <PageTitle>{SPECIALISTS}</PageTitle>
        <Box
          display='flex'
          flexDirection={isMobile ? 'column' : 'row'}
          alignItems='center'
          gap='1.9rem'my='2rem'
        >
          <SpecialistInput maxW='33%'
            specialistName={specialistName}
            handleSpecialistNameChange={handleSpecialistNameChange}
          />
          <SpecialtyInput maxW='33%'
            specialty={specialty}
            handleSpecialtyChange={handleSpecialtyChange} />
          <CityInput maxW='33%'
            city={city}
            handleCityChange={handleCityChange} />
          <Tooltip label='Clear filters'>
            <Box>
              <Icon as={MdFilterAltOff} mt='1.9rem' cursor='pointer' w={8} h={8} onClick={onClearFiltersHandler} />
            </Box>
          </Tooltip>
        </Box>
        {isLoading ? (
        <Box __css={SpecialistsPageTheme.baseStyle}>
          <LoadingSpiner />
        </Box>
      ) : specialists.length > 0 ? (
        <CardWrapper>
          {specialists.map((specialist, index) => (
            <GridItem key={index}>
              <SpecialistCard specialist={specialist} />
            </GridItem>
          ))}
        </CardWrapper>
      ) : (
        <Box __css={SpecialistsPageTheme.baseStyle}>
          <Heading fontSize='20px' textAlign='center'>No results found</Heading>
        </Box>
      )}
        <ScrollToTop />
      </Box>
    </>
  );
};

export default SpecialistsPage;
