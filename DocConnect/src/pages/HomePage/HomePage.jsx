import {Box, GridItem} from '@chakra-ui/react';
import PageTitle from '../../components/PageTitle/PageTitle';
import CardWrapper from '../../components/CardWrapper/CardWrapper';
import {DOCUMENT_TITLE, SPECIALTIES} from '../../common/constants';
import {useSpecialties} from '../../hooks/useSpecialties';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';
import {useTitle} from '../../hooks/useTitle';
import ServerErrorMessage from '../../components/ServerErrorMessage/ServerErrorMessage';
import SpecialtyCard from '../../components/SpecialtyCard/SpecialtyCard';


const HomePage = () => {
  const {specialties, isLoading, error} = useSpecialties();

  useTitle(DOCUMENT_TITLE.HOME_PAGE);

  if (isLoading) return <LoadingSpiner />;
  if (error) return <ServerErrorMessage text={error} />;

  return (
    <Box
      m='4.25rem 3rem'>
      <PageTitle>{SPECIALTIES}</PageTitle>
      <CardWrapper>
        {specialties.map((specialty) => (
          <GridItem key={specialty.id}>
            <SpecialtyCard specialty={specialty} />
          </GridItem>
        ))}
      </CardWrapper>
    </Box>
  );
};

export default HomePage;
