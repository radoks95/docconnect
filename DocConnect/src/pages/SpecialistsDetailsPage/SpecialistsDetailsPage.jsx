import {Box, Image, Text} from '@chakra-ui/react';
import {useParams} from 'react-router-dom';
import {useState, useEffect} from 'react';
import SpecialistsDetailsPageTheme from '../../theme/SpecialistsDetailsPageTheme';
import locationSvg from '../../../public/location.svg';
import {getSpecialistById} from '../../services/specialists.service';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';
import ServerErrorMessage from '../../components/ServerErrorMessage/ServerErrorMessage';
import {useTitle} from '../../hooks/useTitle';
import {DOCUMENT_TITLE} from '../../common/constants';
import ScheduleAppointmentModal from '../../components/ScheduleAppointmentModal/ScheduleAppointmentModal';


const SpecialistsDetailsPage = () => {
  const {id} = useParams();
  const [specialist, setSpecialist] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setIsLoading(true);
    getSpecialistById(id).then((response) => {
      setSpecialist(response);
      setIsLoading(false);
    })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
  }, [id]);

  useTitle(`${DOCUMENT_TITLE.SPECIALIST_DETAILS_PAGE} ${specialist.fullName}`);

  if (isLoading) return <LoadingSpiner />;
  if (error) return <ServerErrorMessage text={error} />;


  return (
    <Box __css={SpecialistsDetailsPageTheme.baseStyle}>
      <Box __css={SpecialistsDetailsPageTheme.profileBox}>
        <Box __css={SpecialistsDetailsPageTheme.portraitBackground}></Box>
        <Box __css={SpecialistsDetailsPageTheme.profileInfo}>
          <Image
            src={specialist.imageUrl}
            alt={specialist.fullName} __css={SpecialistsDetailsPageTheme.specialistImage} />
          <Text as='h2' mt='2rem'
            sx={SpecialistsDetailsPageTheme.specialistNameText}>{specialist.fullName}</Text>
          <Text as='h3' sx={SpecialistsDetailsPageTheme.specialistSpecialtyText}>{specialist.specialityName}</Text>
        </Box>
        <Box __css={SpecialistsDetailsPageTheme.scheduleBox}>
          <Box __css={SpecialistsDetailsPageTheme.locationBox}>
            <Image src={locationSvg} alt='location icon' boxSize='17px' />
            <Text sx={SpecialistsDetailsPageTheme.locationText}>{specialist.address}</Text>
          </Box>
          <ScheduleAppointmentModal id={id} />
        </Box>
      </Box>
      <Box __css={SpecialistsDetailsPageTheme.aboutBox}>
        <Text as='h2' sx={SpecialistsDetailsPageTheme.aboutTitle}>{specialist.fullName}</Text>
        <Text sx={SpecialistsDetailsPageTheme.aboutDescription}>{specialist.summary}</Text>
      </Box>
    </Box>
  );
};

export default SpecialistsDetailsPage;
