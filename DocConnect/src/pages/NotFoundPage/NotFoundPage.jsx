import {Box, Button, Heading, Img, useBreakpointValue} from '@chakra-ui/react';
import BackgroundAuth from '../../components/BackgroundAuth/BackgroundAuth';
import {Link} from 'react-router-dom';
import {HOME_PAGE} from '../../common/routes';
import four from '../../../public/four.svg';
import smallFour from '../../../public/smallFour.svg';
import logo404 from '../../../public/404Logo.svg';
import smallLogo404 from '../../../public/404SmallLogo.svg';
import {useTitle} from '../../hooks/useTitle';
import {DOCUMENT_TITLE} from '../../common/constants';

const NotFoundPage = () => {
  const isMobile = useBreakpointValue({base: true, md: false}, {ssr: false});

  useTitle(DOCUMENT_TITLE.NOT_FOUND_PAGE);

  return (
    <>
      <BackgroundAuth />
      <Box display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
        <Heading
          textAlign='center'
          color='green.600'
          fontSize='24px'
          lineHeight='36px'
          mb='2rem'>
          Oops, <br></br> It appears that the page you are looking for does not exist.<br></br>
         Please navigate back to our homepage in order to be DocConnected again.
        </Heading>
        <Box display='flex' flexDirection='row' zIndex='1' mb='4rem'>
          <Img src={!isMobile ? four : smallFour} />
          <Img src={!isMobile ? logo404 : smallLogo404} />
          <Img src={!isMobile ? four : smallFour} />
        </Box>
        <Link to={HOME_PAGE}>
          <Button
            background='green.700'
            color='white'
            _hover={{
              background: 'green.600',
              transform: 'scale(1.1)',
            }}
          >
            Return Home
          </Button>
        </Link>
      </Box>
    </>
  );
};

export default NotFoundPage;
