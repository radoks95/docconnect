import {useFormik} from 'formik';
import ForgotPassword from '../../components/ForgotPassword/ForgotPassword';
import {forgotPasswordValidationSchema} from '../../schema/forgotPassword.schema';
import {useState} from 'react';
import {DOCUMENT_TITLE} from '../../common/constants';
import {useTitle} from '../../hooks/useTitle';
import {sendEmailForResetPassword} from '../../services/auth.service';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';
import SendEmailForResetPassword from '../../components/SendEmailForResetPassword/SendEmailForResetPassword';


const ForgotPasswordPage = () => {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: forgotPasswordValidationSchema,
    onSubmit: async (values) => {
      try {
        setIsLoading(true);
        await sendEmailForResetPassword(values.email);
        setIsSubmitted(true);
      } catch (error) {
        setIsSubmitted(true);
        setIsLoading(false);
        setError(error.message);
      } finally {
        setIsLoading(false);
      }
    },
  });

  useTitle(DOCUMENT_TITLE.FORGOT_PASSWORD_PAGE);

  if (isLoading) return <LoadingSpiner />;

  return (
    !isSubmitted ? (
      <ForgotPassword formik={formik} />
    ) : (
      <SendEmailForResetPassword error={error} email={formik.values.email} />
    )
  );
};

export default ForgotPasswordPage;
