import SignUp from '../../components/SignUp/SignUp';
import {useFormik} from 'formik';
import {signUpValidationSchema} from '../../schema/signUp.schema';
import {signUpUser} from '../../services/auth.service';
import {useState} from 'react';
import AuthMessage from '../../components/AuthMessage/AuthMessage';
import success from '../../../public/success.svg';
import {CONGRATULATIONS, CONGRATULATIONS_MSG, DOCUMENT_TITLE, LOGIN_LINK} from '../../common/constants';
import {useTitle} from '../../hooks/useTitle';
import {Box} from '@chakra-ui/react';
import ServerErrorMessage from '../../components/ServerErrorMessage/ServerErrorMessage';
import {LOGIN_PAGE} from '../../common/routes';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';

const SignUpPage = () => {
  const [isSignedUp, setIsSignedUp] = useState(false);
  const [serverError, setServerError] = useState(null);
  const [isLoadingSignUp, setIsLoadingSignUp] = useState(false);

  const formik = useFormik({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      confirmPassword: '',
      confirmAge: false,
      confirmPolicy: false,
      confirmTerms: false,
    },
    validationSchema: signUpValidationSchema,
    onSubmit: async (values) => {
      const credentials = {
        email: values.email,
        firstName: values.firstName,
        lastName: values.lastName,
        password: values.password,
        confirmPassword: values.confirmPassword,
      };
      try {
        setIsLoadingSignUp(true);
        await signUpUser(credentials);
        setIsSignedUp(true);
      } catch (error) {
        setServerError(error.message);
      } finally {
        setIsLoadingSignUp(false);
      }
    },
  });

  useTitle(DOCUMENT_TITLE.SIGN_UP_PAGE);

  if (isLoadingSignUp) return <LoadingSpiner />;

  return (
    !isSignedUp ? (
      <Box display="flex" flexDirection="column" justifyContent='center' alignItems='center' zIndex='2'>
        {serverError && <ServerErrorMessage text={serverError} />}
        <Box marginTop={serverError ? '-2.5rem' : 0}>
          <SignUp formik={formik} />
        </Box>
      </Box>
    ) : (
      <AuthMessage
        image={success}
        alt={'Success Icon'}
        heading={CONGRATULATIONS}
        text={CONGRATULATIONS_MSG}
        linkTo={LOGIN_PAGE}
        link={LOGIN_LINK} />
    )
  );
};

export default SignUpPage;
