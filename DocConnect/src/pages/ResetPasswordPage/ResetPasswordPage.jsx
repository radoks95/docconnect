import {useFormik} from 'formik';
import ResetPassword from '../../components/ResetPassword/ResetPassword';
import BackgroundAuth from '../../components/BackgroundAuth/BackgroundAuth';
import {resetPasswordValidationSchema} from '../../schema/resetPassword.schema';
import {useTitle} from '../../hooks/useTitle';
import {DOCUMENT_TITLE} from '../../common/constants';
import {useState} from 'react';
import {resetPassword} from '../../services/auth.service';
import {useParams} from 'react-router-dom';
import ResetPasswordMassege from '../../components/ResetPasswordMassage/ResetPasswordMassege';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';

const ResetPasswordPage = () => {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const {email, token} = useParams();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const formik = useFormik({
    initialValues: {
      newPassword: '',
      confirmPassword: '',
    },
    validationSchema: resetPasswordValidationSchema,
    onSubmit: async (values) => {
      try {
        setIsLoading(true);
        await resetPassword(email, token, values);
        setIsSubmitted(true);
      } catch (error) {
        setIsSubmitted(true);
        setIsLoading(false);
        setError(error.message);
      } finally {
        setIsLoading(false);
      }
    },
  });

  useTitle(DOCUMENT_TITLE.RESET_PASSWORD_PAGE);

  if (isLoading) return <><BackgroundAuth /><LoadingSpiner /></>;

  return (
    <>
      <BackgroundAuth />
      {!isSubmitted ? (
          <ResetPassword formik={formik} />
      ) : (
       <ResetPasswordMassege error={error}/>
      )}
    </>
  );
};

export default ResetPasswordPage;
