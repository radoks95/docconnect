import {useFormik} from 'formik';
import {useState} from 'react';
import Login from '../../components/Login/Login';
import {loginValidationSchema} from '../../schema/login.schema';
import {loginUser, resendEmailVerification} from '../../services/auth.service';
import {DOCUMENT_TITLE} from '../../common/constants';
import {useTitle} from '../../hooks/useTitle';
import {useNavigate} from 'react-router';
import {HOME_PAGE} from '../../common/routes';
import jwt from 'jwt-decode';
import Cookies from 'js-cookie';
import {useDispatch} from 'react-redux';
import {setUser} from '../../slices/userSlice';
import LoadingSpiner from '../../components/LoadingSpiner/LoadingSpiner';
import ResendEmail from '../../components/ResendEmail/ResendEmail';
import {useShowToast} from '../../hooks/useShowToast';

const LoginPage = () => {
  const [serverError, setServerError] = useState(null);
  const [isRequestedResend, setIsRequestedResend] = useState(false);
  const [isLoadingResend, setIsLoadingResend] = useState(false);
  const [errorResend, setErrorResend] = useState(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const showToast = useShowToast();


  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: loginValidationSchema,
    onSubmit: async (values) => {
      try {
        const response = await loginUser(values);
        const decodedAccessToken = jwt(response.accessTokenResultDTO.value);
        const decodedRefreshToken = jwt(response.refreshTokenResultDTO.value);
        Cookies.set('jwt_auth', response.accessTokenResultDTO.value, {
          expires: new Date(decodedAccessToken.exp * 1000),
          secure: true,
          sameSite: 'strict'});
        Cookies.set('ref_auth', response.refreshTokenResultDTO.value, {
          expires: new Date(decodedRefreshToken.exp * 1000),
          secure: true,
          sameSite: 'strict'});
        dispatch(setUser({
          id: decodedAccessToken.sub,
          email: decodedAccessToken.email,
          accessTokenExpireIn: decodedAccessToken.exp,
          refreshTokenExpireIn: decodedRefreshToken.exp,
          firstName: decodedAccessToken.firstName,
          lastName: decodedAccessToken.lastName,
        }));
        navigate(HOME_PAGE);
        showToast('Success', 'You have logged in successfully', 'success');
      } catch (error) {
        setServerError(error.message);
      }
    },
  });

  useTitle(DOCUMENT_TITLE.LOGIN_PAGE);

  const handleResendVerification = async () => {
    try {
      setIsLoadingResend(true);
      await resendEmailVerification(formik.values.email);
      setIsRequestedResend(true);
    } catch (error) {
      setIsRequestedResend(true);
      setErrorResend(error.message);
    } finally {
      setIsLoadingResend(false);
    }
  };

  if (isLoadingResend) return <LoadingSpiner />;


  return (
    !isRequestedResend ? (
      <Login formik={formik} serverError={serverError} handleResendVerification={handleResendVerification} />
    ) : (
      <ResendEmail errorResend={errorResend}/>
    )
  );
};

export default LoginPage;
