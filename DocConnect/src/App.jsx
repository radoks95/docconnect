import {Route, Routes, useMatch} from 'react-router-dom';
import * as ROUTES from './common/routes';
import {Suspense, lazy} from 'react';
import Logged from './hoc/Logged';
import Layout from './components/Layout/Layout';
import NavBar from './components/NavBar/NavBar';
import BackgroundAuth from './components/BackgroundAuth/BackgroundAuth';
import LoadingSpiner from './components/LoadingSpiner/LoadingSpiner';
import NotLogged from './hoc/NotLogged';

const HomePage = lazy(() => import('./pages/HomePage/HomePage'));
const SpecialistsPage = lazy(() => import('./pages/SpecialistsPage/SpecialistsPage'));
const SignUpPage = lazy(() => import('./pages/SignUpPage/SignUpPage'));
const LoginPage = lazy(() => import('./pages/LoginPage/LoginPage'));
const ForgotPasswordPage = lazy(() => import('./pages/ForgotPasswordPage/ForgotPasswordPage'));
const ResetPasswordPage = lazy(() => import('./pages/ResetPasswordPage/ResetPasswordPage'));
const VerifiedEmailPage = lazy(() => import('./pages/VerifiedEmailPage/VerifiedEmailPage'));
const AppointmentsPage = lazy(() => import('./pages/AppointmentsPage/AppointmentsPage'));
const NotFoundPage = lazy(() => import('./pages/NotFoundPage/NotFoundPage'));
const SpecialistsDetailsPage = lazy(()=> import('./pages/SpecialistsDetailsPage/SpecialistsDetailsPage'));

const App = () => {
  const matchSignUp = useMatch(ROUTES.SIGN_UP_PAGE);
  const matchLogin = useMatch(ROUTES.LOGIN_PAGE);
  const matchForgotPassword = useMatch(ROUTES.FORGOT_PASSWORD);

  const match = matchSignUp || matchLogin || matchForgotPassword;


  return (
    <>
      <NavBar />
      <Layout>
        {match && <BackgroundAuth/>}
        <Suspense fallback={<LoadingSpiner/>}>
          <Routes>
            <Route path={ROUTES.HOME_PAGE} element={<HomePage />} />
            <Route path={ROUTES.SPECIALTY_SPECIALISTS_PAGE} element={<SpecialistsPage />} />
            <Route path={ROUTES.SPECIALISTS_PAGE} element={<SpecialistsPage />} />
            <Route path={ROUTES.SPECIALIST_PROFILE_PAGE} element={<SpecialistsDetailsPage />} />
            <Route path={ROUTES.SIGN_UP_PAGE} element={<NotLogged><SignUpPage /></NotLogged>} />
            <Route path={ROUTES.LOGIN_PAGE} element={<NotLogged><LoginPage /></NotLogged>} />
            <Route path={ROUTES.FORGOT_PASSWORD} element={<NotLogged><ForgotPasswordPage /></NotLogged>} />
            <Route path={ROUTES.RESET_PASSWORD} element={<NotLogged><ResetPasswordPage /></NotLogged>} />
            <Route path={ROUTES.EMAIL_VERIFICATION_PAGE} element={<NotLogged><VerifiedEmailPage /></NotLogged>} />
            <Route path={ROUTES.APPOINTMENTS_PAGE} element={<Logged><AppointmentsPage /></Logged>} />
            <Route path={ROUTES.NOT_FOUND_PAGE} element={<NotFoundPage />} />
          </Routes>
        </Suspense>
      </Layout>
    </>
  );
};

export default App;
