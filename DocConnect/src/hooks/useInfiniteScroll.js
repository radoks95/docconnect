import {useCallback, useEffect} from 'react';

export const useInfiniteScroll = (setPage) => {
  const handleScroll = useCallback((e) => {
    e.preventDefault();
    if (
      window.innerHeight + e.target.documentElement.scrollTop + 1 >=
          e.target.documentElement.scrollHeight
    ) {
      setPage((prevPage) => prevPage + 1);
    }
  }, []);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);
};
