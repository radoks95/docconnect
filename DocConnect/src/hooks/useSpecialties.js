import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {fetchAllSpecialties} from '../slices/specialtiesSlice';

export const useSpecialties = () => {
  const dispatch = useDispatch();
  const specialties = useSelector((state) => state.specialties.specialties);
  const isLoading = useSelector((state) => state.specialties.isLoadingSpecialties);
  const error = useSelector((state) => state.specialties.errorSpecialties);

  useEffect(() => {
    dispatch(fetchAllSpecialties());
  }, [dispatch]);

  return {specialties, isLoading, error};
};
