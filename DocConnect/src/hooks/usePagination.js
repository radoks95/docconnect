import {useEffect, useState} from 'react';

export const usePagination = (data) => {
  const itemsPerPage = 4;
  const [currentPage, setCurrentPage] = useState(0);
  const [pageCount, setPageCount] = useState(1);

  useEffect(() => {
    const newPageCount = Math.ceil(data.length / itemsPerPage);
    setPageCount(newPageCount);
  }, [data]);

  const handleNextPage = () => {
    if (currentPage < pageCount - 1) {
      setCurrentPage((prevPage) => prevPage + 1);
    }
  };

  const handlePrevPage = () => {
    if (currentPage > 0) {
      setCurrentPage((prevPage) => prevPage - 1);
    }
  };

  const handlePageClick = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const slicedData = data.slice(
      currentPage * itemsPerPage,
      (currentPage + 1) * itemsPerPage,
  );

  const pageNumbers = Array.from({length: pageCount}, (_, i) => i);

  return {
    slicedData,
    pageNumbers,
    currentPage,
    pageCount,
    handleNextPage,
    handlePrevPage,
    handlePageClick,
  };
};
