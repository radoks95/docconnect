import {useToast} from '@chakra-ui/react';

export const useShowToast = () => {
  const toast = useToast();

  const showToast = (title, description, status) => {
    toast({
      title: title,
      description: description,
      duration: 5000,
      status: status,
      isClosable: true,
      position: 'top',
      containerStyle: {
        mt: '5rem',
      },
    });
  };

  return showToast;
};
