import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {fetchAllSpecialists, specialistActions} from '../slices/specialistsSlice';
export const useSpecialists = (page, setPage) => {
  const dispatch = useDispatch();
  const specialists = useSelector((state) => state.specialists.specialists);
  const isLoading = useSelector((state) => state.specialists.isLoadingSpecialists);
  const error = useSelector((state) => state.specialists.errorSpecialists);
  const specialistName = useSelector((state) => state.specialists.specialistName);
  const specialty = useSelector((state) => state.specialists.specialty);
  const city = useSelector((state) => state.specialists.city);

  useEffect(() => {
    setPage(1);
    dispatch(specialistActions.setEmptySpecialists());
  }, [dispatch, specialistName, specialty, city, setPage]);
  useEffect(() => {
    dispatch(fetchAllSpecialists({specialistName, specialty, city, page}));
  }, [dispatch, specialistName, specialty, city, page]);

  return {specialists, isLoading, error};
};
