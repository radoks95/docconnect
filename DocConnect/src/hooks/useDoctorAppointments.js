import {useEffect, useState} from 'react';
import {getDoctorsAppointmentsById} from '../services/appointments.service';


export const useDoctorAppointments = (id) => {
  const [appointments, setAppointments] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setIsLoading(true);
    getDoctorsAppointmentsById(id).then((response) => {
      setAppointments(response);
      setIsLoading(false);
    })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
  }, [id]);

  return {appointments, isLoading, error};
};
