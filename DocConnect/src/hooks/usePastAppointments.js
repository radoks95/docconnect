import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {fetchAllPastAppointments} from '../slices/pastAppointmentsSlice';

export const usePastAppointments = (id) => {
  const dispatch = useDispatch();
  const pastAppointments = useSelector((state) => state.pastAppointments.pastAppointments);
  const isLoading = useSelector((state) => state.pastAppointments.isLoadingPastAppointments);
  const error = useSelector((state) => state.pastAppointments.errorPastAppointments);

  useEffect(() => {
    dispatch(fetchAllPastAppointments(id));
  }, [dispatch, id]);

  return {pastAppointments, isLoading, error};
};
