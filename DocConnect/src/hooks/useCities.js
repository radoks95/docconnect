import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {fetchAllCities} from '../slices/citiesSlice';

export const useCities = () => {
  const dispatch = useDispatch();
  const cities = useSelector((state) => state.cities.cities);
  const isLoading = useSelector((state) => state.cities.isLoadingCities);
  const error = useSelector((state) => state.cities.errorCities);

  useEffect(() => {
    dispatch(fetchAllCities());
  }, [dispatch]);

  return {cities, isLoading, error};
};
