import {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {useSearchParams} from 'react-router-dom';
import {specialistActions} from '../slices/specialistsSlice';
import {getAllSpecialists} from '../services/specialists.service';
import {useDebounce} from './useDebounce';

export const useFilterSpecialists = () => {
  const dispatch = useDispatch();
  const [searchParams, setSearchParams] = useSearchParams();
  const [specialistName, setSpecialistName] = useState(searchParams.get('specialistName') || '');
  const [specialty, setSpecialty] = useState(searchParams.get('specialty') || '');
  const [city, setCity] = useState(searchParams.get('city') || '');
  const debouncedName = useDebounce(specialistName, 500);
  const debouncedSpecialty = useDebounce(specialty, 500);
  const debouncedCity = useDebounce(city, 500);

  useEffect(() => {
    dispatch(specialistActions.setSpecialistName(debouncedName));
    dispatch(specialistActions.setSpecialty(debouncedSpecialty));
    dispatch(specialistActions.setCity(debouncedCity));

    const fetchSpecialists = async () => {
      try {
        const response = await getAllSpecialists(debouncedName, debouncedSpecialty, debouncedCity);
        dispatch(specialistActions.setSpecialists(response));
        setSearchParams({specialistName: debouncedName, specialty: debouncedSpecialty, city: debouncedCity});
      } catch (error) {
        console.error('Failed to fetch specialists', error);
      }
    };

    fetchSpecialists();
  }, [dispatch, debouncedName, debouncedSpecialty, debouncedCity, setSearchParams]);

  const handleSpecialistNameChange = (e) => {
    setSpecialistName(e.target.value);
  };

  const handleSpecialtyChange = (e) => {
    setSpecialty(e.label);
  };

  const handleCityChange = (e) => {
    setCity(e.label);
  };

  const onClearFiltersHandler = (e) => {
    e.preventDefault();
    setSearchParams({specialistName: '', specialty: ''});
    setSpecialistName('');
    setSpecialty('');
    setCity('');
  };

  return {
    specialistName,
    specialty,
    city,
    handleSpecialistNameChange,
    handleSpecialtyChange,
    handleCityChange,
    onClearFiltersHandler,
  };
};
