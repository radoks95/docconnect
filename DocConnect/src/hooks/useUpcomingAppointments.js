import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {fetchAllUpcomingAppointments} from '../slices/upcomingAppointmentSlice';

export const useUpcomingAppointments = (id) => {
  const dispatch = useDispatch();
  const upcomingAppointments = useSelector((state) => state.upcomingAppointments.upcomingAppointments);
  const isLoading = useSelector((state) => state.upcomingAppointments.isLoadingUpcomingAppointments);
  const error = useSelector((state) => state.upcomingAppointments.errorUpcomingAppointments);

  useEffect(() => {
    dispatch(fetchAllUpcomingAppointments(id));
  }, [dispatch, id]);

  return {upcomingAppointments, isLoading, error};
};
