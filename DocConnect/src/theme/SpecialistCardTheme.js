const baseStyle = {
  w: {
    'base': '20rem',
    'sm': '22rem',
    '2xl': '25rem',
  },
  borderRadius: '1rem',
  border: '2px solid rgba(0, 0, 0, 0.15)',
  boxShadow: '2px 2px 6px 0px rgba(0, 0, 0, 0.25)',
  overflow: 'hidden',
  bg: 'white',
  cursor: 'pointer',
  _hover: {
    boxShadow: '2px 2px 6px 0px rgba(0, 0, 0, 0.8)',
    transform: 'scale(1.04)',
  },
};

const imageBox = {
  position: 'absolute',
  bottom: '0',
  left: '0',
  w: '100%',
  h: '40%',
  bgGradient: 'linear(to bottom, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1))',
};

const headingBox = {
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  height: '5rem',
  padding: '0.8rem 1.6rem',
  alignItems: 'flex-start',
  flexShrink: '0',
};

const locationBox = {
  display: 'flex',
  flexDirection: 'row',
  width: '80%',
  padding: '0 1.6rem',
  alignItems: 'flex-start',
  gap: '5px',
};

const text = {
  fontFamily: 'Inter',
  fontSize: '16px',
  fontStyle: 'normal',
  fontWeight: '400',
  lineHeight: 'normal',
};

const buttonStyle = {
  w: '100%',
  h: '2.7rem',
  bg: 'green.800',
  color: 'white',
  borderRadius: '5px',
  _hover: {bg: 'green.900', color: 'green.100', border: '2px solid #C6F6D5'},
};

const SpecialistCardTheme = {
  baseStyle,
  imageBox,
  headingBox,
  locationBox,
  text,
  buttonStyle,
};

export default SpecialistCardTheme;


