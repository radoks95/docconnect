const baseStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  height: '4rem',
  padding: '2.2rem 6rem',
  backgroundColor: 'green.900',
  position: 'sticky',
  top: '0',
  zIndex: '9999999',
  width: '100%',
};

const NavBarTheme = {
  baseStyle: baseStyle,
  mobileStyle: {
    ...baseStyle,
    padding: '0.8em 1rem',
  },
  navButtons: {
    display: 'flex',
    justifyContent: 'space-between',
    gap: '1.6rem',
    h: '1.7rem',
  },
};

export default NavBarTheme;
