const baseStyle = {
  display: 'flex',
  flexDirection: 'column',
  maxWidth: '90rem',
  gap: '34px',
  padding: ['61px 0 273px 0', '61px 0 273px 0'],
  justifyContent: 'center',
  alignItems: 'center',
};

const profileBox = {
  width: ['314px', '426px', '720px', '880px', '1034px'],
  height: ['490px', '490px', '363px', '363px'],
  flexShrink: '0',
  borderRadius: '15px',
  background: '#FFF',
  boxShadow: '2px 2px 6px 0px rgba(0, 0, 0, 0.25)',
};

const portraitBackground = {
  width: ['314px', '426px', '720px', '880px', '1034px'],
  height: '148px',
  flexShrink: '0',
  background: 'linear-gradient(90deg, #51b122 15.48%, #307757 100%)',
  borderRadius: '15px 15px 0px 0px',
};

const profileInfo = {
  width: '217px',
  height: '319px',
  position: 'absolute',
  transform: [
    'translate(2.8rem, -7.5rem)',
    'translate(45%, -7.5rem)',
    'translate(3rem, -7rem)',
    'translate(3rem, -7rem)',
  ],
};

const specialistImage = {
  boxSize: '217px',
  borderRadius: '50%',
  border: '10px solid var(--White, #FFFFFD)',
  objectFit: 'cover',
  objectPosition: 'center',
};

const specialistNameText = {
  w: '16rem',
  color: 'green.900',
  textAlign: 'left',
  fontSize: '24px',
  fontStyle: 'normal',
  fontWeight: '700',
  lineHeight: 'normal',
};

const specialistSpecialtyText = {
  color: 'green.600',
  textAlign: 'left',
  fontSize: '16px',
  fontStyle: 'normal',
  fontWeight: '400',
  lineHeight: 'normal',
};

const scheduleBox = {
  display: 'flex',
  flexDirection: 'column',
  width: ['217px', '217px', '320px', '420px', '480px'],
  gap: ['15px', '15px', '30px', '30px', '30px'],
  position: 'relative',
  transform: [
    'translate(2.8rem, 12rem)',
    'translate(45%, 12rem)',
    'translate(20rem, 3rem)',
    'translate(20rem, 3rem)',
    'translate(25rem, 3rem)',
  ],
};

const scheduleButton = {
  backgroundColor: 'green.800',
  color: '#FFF2F8',
  maxWidth: '278px',
  borderRadius: '5px',
};

const locationBox = {
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  gap: '0.5rem',
};

const locationText = {
  color: 'green.900',
  textAlign: 'left',
  fontSize: '16px',
  fontStyle: 'normal',
  fontWeight: '400',
  lineHeight: 'normal',
};

const aboutBox = {
  display: 'flex',
  width: ['314px', '426px', '720px', '880px', '1034px'],
  padding: '31px',
  flexDirection: 'column',
  alignItems: 'flex-start',
  gap: '31px',
  borderRadius: '15px',
  background: '#FFFFFD',
  boxShadow: '2px 2px 6px 0px rgba(0, 0, 0, 0.25)',
};

const aboutTitle = {
  color: 'green.900',
  textAlign: 'left',
  fontSize: '20px',
  fontStyle: 'normal',
  fontWeight: '700',
  lineHeight: 'normal',
};

const aboutDescription = {
  textAlign: 'left',
  fontSize: '16px',
  fontStyle: 'normal',
  fontWeight: '400',
  lineHeight: 'normal',
};

const SpecialistsDetailsPageTheme = {
  baseStyle,
  profileBox,
  aboutBox,
  portraitBackground,
  profileInfo,
  specialistImage,
  specialistNameText,
  specialistSpecialtyText,
  aboutTitle,
  aboutDescription,
  locationBox,
  locationText,
  scheduleBox,
  scheduleButton,
};

export default SpecialistsDetailsPageTheme;
