const baseStyle = {
  w: {base: '20rem', sm: '20rem', md: '46rem', lg: '48rem', xl: '83rem'},
  h: '21rem%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  mx: 'auto',
  mb: '100vh',
  pt: {md: '5rem', lg: '10rem'},
};


const SpecialistsPageTheme = {
  baseStyle,

};

export default SpecialistsPageTheme;


