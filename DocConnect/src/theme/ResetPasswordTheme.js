const baseStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  bg: 'white',
  gap: '1.8rem',
  m: '1rem 0',
  p: '2.4rem 4.3rem',
  borderRadius: '1rem',
  boxShadow: '0px 4px 4px 0px rgba(0, 0, 0, 0.25)',
  maxW: '25rem',
  minW: '25rem',
  zIndex: 999,
};

const boxStyle = {
  gap: '5px',
  alignSelf: 'stretch',
  mb: '1.2rem',
  w: '17rem',
};

const buttonStyle = {
  w: '100%',
  h: '2.7rem',
  type: 'submit',
  bg: 'green.800',
  color: 'white',
  borderRadius: '5px',
  _hover: {bg: 'green.900', color: 'green.100', border: '2px solid #C6F6D5'},
};


const ResetPasswordTheme = {
  baseStyle,
  baseStyleMobile: {
    ...baseStyle,
    minW: '17rem',
    p: '1.4rem 2.3rem',
  },
  boxStyle,
  boxStyleMobile: {
    ...boxStyle,
    w: '17rem',
  },
  buttonStyle,
};

export default ResetPasswordTheme;


