const baseStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  gap: '1.8rem',
  w: '30rem',
  p: '2.4rem 3rem',
  background: 'white',
  borderRadius: '1rem',
  boxShadow: '0px 4px 4px 0px rgba(0, 0, 0, 0.25)',
  zIndex: '999',
};

const wrapperStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  alignSelf: 'stretch',
  gap: '1.25rem',
};

const AuthMessageTheme = {
  baseStyle,
  wrapperStyle,
};

export default AuthMessageTheme;
