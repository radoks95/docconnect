const baseStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  bg: 'white',
  gap: ['1rem', '1.8rem'],
  m: '1rem 0',
  borderRadius: '1rem',
  boxShadow: '0px 4px 4px 0px rgba(0, 0, 0, 0.25)',
  maxW: '30rem',
  maxH: '52.2rem',
  zIndex: 999,
};

const boxStyle = {
  gap: '5px',
  alignSelf: 'stretch',
  mb: '1.2rem',
};

const buttonStyle = {
  w: '100%',
  h: '2.7rem',
  type: 'submit',
  bg: 'green.800',
  color: 'white',
  borderRadius: '5px',
  _hover: {bg: 'green.900', color: 'green.100', border: '2px solid #C6F6D5'},
};

const LoginTheme = {
  baseStyle,
  boxStyle,
  buttonStyle,
};

export default LoginTheme;
