import {extendTheme} from '@chakra-ui/react';
import NavBarTheme from './NavBarTheme';
import NavAuthTheme from './NavAuthTheme';
import NavDrawerTheme from './NavDrawerTheme';
import SpecialtyCardTheme from './SpecialtyCardTheme';
import SignUpTheme from './SignUpTheme';
import AuthMessageTheme from './AuthMessageTheme';
import LoginTheme from './LoginTheme';
import ForgotPasswordTheme from './ForgotPasswordTheme';
import ResetPasswordTheme from './ResetPasswordTheme';
import SpecialistsDetailsPageTheme from './SpecialistsDetailsPageTheme';
import SpecialistsPageTheme from './SpecialistsPageTheme';

const theme = extendTheme({
  components: {
    NavBar: NavBarTheme,
    NavAuth: NavAuthTheme,
    NavDrawer: NavDrawerTheme,
    SpecialtyCard: SpecialtyCardTheme,
    SignUp: SignUpTheme,
    Login: LoginTheme,
    AuthMessage: AuthMessageTheme,
    ForgotPassword: ForgotPasswordTheme,
    ResetPassword: ResetPasswordTheme,
    SpecialistsDetailsPageTheme: SpecialistsDetailsPageTheme,
    SpecialistPage: SpecialistsPageTheme,
  },
});

export default theme;
