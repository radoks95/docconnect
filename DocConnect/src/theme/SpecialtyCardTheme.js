const baseStyle = {
  w: {
    'base': '20rem',
    'sm': '22rem',
    '2xl': '25rem',
  },
  h: '27rem',
  borderRadius: '1rem',
  border: '2px solid rgba(0, 0, 0, 0.15)',
  boxShadow: '2px 2px 6px 0px rgba(0, 0, 0, 0.25)',
  overflow: 'hidden',
  bg: 'white',
  cursor: 'pointer',
  _hover: {
    boxShadow: '2px 2px 6px 0px rgba(0, 0, 0, 0.8)',
    transform: 'scale(1.04)',
  },
};

const imageBox = {
  position: 'absolute',
  bottom: '0',
  left: '0',
  w: '100%',
  h: '40%',
  bgGradient: 'linear(to bottom, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1))',
};

const headingBox = {
  display: 'flex',
  width: '100%',
  height: '5rem',
  padding: '0.8rem 1rem',
  alignItems: 'flex-end',
  gap: '0.6rem',
  flexShrink: '0',
};


const SpecialtyCardTheme = {
  baseStyle,
  imageBox,
  headingBox,
};

export default SpecialtyCardTheme;

