const baseStyle = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  mt: '2rem',
  gap: '1rem',
};


const NavDrawerTheme = {
  baseStyle,
};

export default NavDrawerTheme;


