const baseStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  gap: '1.6rem',
  h: '1.7rem',
  color: '#FFF2F8',
  fontSize: '16px',
};

const buttonStyle = {
  colorScheme: 'green',
  h: '1.75rem',
  w: '5.3rem',
  border: '1px',
  borderRadius: '5px',
  bg: 'green.100',
  color: 'green.800',
  _hover: {
    color: '#FFF2F8',
    backgroundColor: 'green.200',
    transform: 'scale(1.1)',
  },
};

const NavAuthTheme = {
  baseStyle,
  buttonStyle,
  matchSignUp: {
    ...buttonStyle,
    bg: 'green.500',
    color: '#FFF2F8',
  },
  matchLogin: {
    color: 'green.200',
    textDecoration: 'underline',
  },
};

export default NavAuthTheme;

