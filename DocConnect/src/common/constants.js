export const SPECIALTIES = 'Specialties';
export const SPECIALISTS = 'Specialists';
export const APPOINTMENTS = 'Appointments';
export const LOGIN = 'Login';
export const SIGN_UP = 'Sign up';
export const MAX_NAME_LENGTH = 50;
export const MIN_PASSWORD_LENGTH = 8;
export const MAX_PASSWORD_LENGTH = 100;
export const SUPPORT_EMAIL = 'support@docconnect-green.devsmm.com';


export const ALL_SPECIALTIES_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Speciality`;
export const ALL_SPECIALISTS_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Doctor/search-models`;
export const ALL_CITIES_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Location/detailed-location`;
export const SIGN_UP_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/sign-up`;
export const LOGIN_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/login`;
export const LOGOUT_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/log-out`;
export const REQUEST_ACCESS_TOKEN_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/request-access-token`;
export const VERIFY_EMAIL_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/confirm-email`;
export const RESEND_EMAIL_VERIFICATION_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/resend-verification-email`;
export const SEND_EMAIL_RESET_PASSWORD = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/forgot-password`;
export const SPECIALIST_BY_ID_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Doctor/detailed-info`;
export const RESET_PASSWORD_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Authorize/reset-password`;
export const DOCTOR_APPOINTMENTS_BY_ID_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Appointment/doctor`;
export const CREATE_APPOINTMENT_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Appointment`;
export const UPCOMING_APPOINTMENTS_BY_ID_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Appointment/futute-patient-appointments`;
export const PAST_APPOINTMENTS_BY_ID_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Appointment/past-patient-appointments`;
export const DELETE_APPOINTMENT_URL = `${import.meta.env.VITE_BACKEND_API_HOSTNAME}/api/Appointment`;

export const ERROR_MESSAGE = {
  SPECIALTIES: 'Error fetching specialties',
  SPECIALISTS: 'Error fetching specialists',
  CITIES: 'Error fetching cities',
  VERIFY_USER_AFTER_SIGN_UP: 'Failed to verify user',
  REQUEST_ACCESS_TOKEN: 'Invalid token',
  SPECIALIST: 'Error fetching specialist information',
};
export const DOCUMENT_TITLE = {
  HOME_PAGE: 'DocConnect - Welcome to our home page',
  SIGN_UP_PAGE: 'DocConnect - Sign up',
  LOGIN_PAGE: 'DocConnect - Login',
  FORGOT_PASSWORD_PAGE: 'DocConnect - Forgot Password',
  RESET_PASSWORD_PAGE: 'DocConnect - Reset Password',
  VERIFIED_EMAIL_PAGE: 'DocConnect - Email Verification',
  SPECIALISTS_PAGE: 'DocConnect - Specialists',
  APPOINTMENTS_PAGE: 'DocConnect - Appointments',
  NOT_FOUND_PAGE: '404 - Not Found - DocConnect',
  SPECIALIST_DETAILS_PAGE: 'DocConnect - Specialist -',
};

export const CONGRATULATIONS = 'Congratulations!';
export const CONGRATULATIONS_MSG = `Your account has been created successfully. \n
An email was sent to verify your account, please check your email`;
export const CONGRATULATIONS_RESET_MSG = 'Your password has been successfully reset.';

export const LOGIN_LINK = 'Go to the Login page';
export const PRIVACY_POLICY = 'Privacy Policy';
export const PRIVACY_POLICY_TEXT = 'I have read and agree to the ';
export const TERMS_AND_CONDITIONS = 'Terms and Conditions';
export const TERMS_AND_CONDITIONS_TEXT = 'I have read and agree to the ';

export const FORGOT_PASSWORD = 'Forgot Password';
export const SUCCESSFULLY_MADE_APPOINTMENT =
'You have successfully created an appointment. You can see them in Appointments page.';
