import Cookies from 'js-cookie';
import jwt from 'jwt-decode';
import {clearUser} from '../slices/userSlice';
import {logoutUser, requestAccessToken} from '../services/auth.service';
export const PASSWORD_REG_EXPRESSION = /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+,-./:;<=>?@[\\\]^_`{|}~ ", `,' ]).+$/;
export const EMAIL_REG_EXPRESSION = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;


export const handleLogout = async (dispatch) => {
  const accessToken = Cookies.get('jwt_auth');
  const refreshToken = Cookies.get('ref_auth');
  accessToken && refreshToken && await logoutUser(accessToken, refreshToken);
  Cookies.remove('jwt_auth');
  Cookies.remove('ref_auth');
  dispatch(clearUser());
};

export const requestNewAccessToken = async () => {
  const accessToken = Cookies.get('jwt_auth');
  const refreshToken = Cookies.get('ref_auth');
  const response = await requestAccessToken(accessToken, refreshToken);
  const decodedAccessToken = jwt(response.value);
  Cookies.set('jwt_auth', response.value, {
    expires: new Date(decodedAccessToken.exp * 1000),
    secure: true,
    sameSite: 'strict'});
};

export const getNextDays = (numberOfDays) => {
  const days = [];
  const tomorrow = new Date();

  while (days.length < numberOfDays) {
    tomorrow.setDate(tomorrow.getDate() + 1);
    days.push(new Date(tomorrow));
  }

  return days;
};

export const calculateReservedDays = (appointments) => {
  return appointments?.map((app) => {
    const date = new Date(app.timeSlot);
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const hours = date.getHours();
    const minutes = date.getMinutes();

    return `${month}-${day}-${year}T${hours}:${minutes.toString().padStart(2, '0')}`;
  }) || [];
};

export const formatDate = (inputDate) => {
  const parts = inputDate.split(' ');
  const [dateStr, timeStr] = parts;

  const [month, day, year] = dateStr.split('-');
  const [hour, minute] = timeStr.split(':');

  const newDate = new Date(Date.UTC(year, month - 1, day, hour, minute));

  const formattedDate = newDate.toISOString().substring(0, 19);

  return formattedDate;
};

export const getAppointmentDate = (date)=>{
  const inputDateTime = new Date(date);
  const formattedDate = `${(inputDateTime.getMonth() + 1).toString().padStart(2, '0')}/` +
                       `${inputDateTime.getDate().toString().padStart(2, '0')}/` +
                       `${(inputDateTime.getFullYear() % 100).toString().padStart(2, '0')}`;

  const formattedTime = `${inputDateTime.getHours().toString().padStart(2, '0')}:` +
                       `${inputDateTime.getMinutes().toString().padStart(2, '0')}`;

  return {formattedDate, formattedTime};
};
