import {ALL_SPECIALISTS_URL, ERROR_MESSAGE, SPECIALIST_BY_ID_URL} from '../common/constants';


export const getAllSpecialists = async (name = '', specialty = '', city = '', amount = 18, page = 1) => {
  try {
    const data = await fetch(`${ALL_SPECIALISTS_URL}/
    ${amount}/${page}?name=${name}&specialityName=${specialty}&locationName=${city}`);

    if (!data.ok) {
      throw new Error('Failed to fetch specialties');
    }
    return data.json();
  } catch (error) {
    console.error(ERROR_MESSAGE.SPECIALISTS, error);
    throw ERROR_MESSAGE.SPECIALISTS;
  }
};

export const getSpecialistById = async (id) => {
  try {
    const data = await fetch(`${SPECIALIST_BY_ID_URL}/${id}`);

    if (!data.ok) {
      throw new Error('Failed to fetch specialist information');
    }
    return data.json();
  } catch (error) {
    console.error(ERROR_MESSAGE.SPECIALIST, error);
    throw ERROR_MESSAGE.SPECIALIST;
  }
};
