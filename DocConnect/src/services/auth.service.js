import {
  ERROR_MESSAGE,
  RESEND_EMAIL_VERIFICATION_URL,
  SEND_EMAIL_RESET_PASSWORD,
  SIGN_UP_URL, VERIFY_EMAIL_URL,
  RESET_PASSWORD_URL,
} from '../common/constants';
import {LOGIN_URL, LOGOUT_URL} from '../common/constants';

export const signUpUser = async (credentials) => {
  const response = await fetch(SIGN_UP_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(credentials),
  });

  if (!response.ok) {
    const errorData = await response.json();
    const error = errorData[0] ? errorData : (Object.values(errorData.errors)[0]);
    throw new Error(error);
  }
};

export const loginUser = async (credentials) => {
  const response = await fetch(LOGIN_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(credentials),
  });

  if (!response.ok) {
    const errorData = await response.json();
    throw new Error(errorData);
  }

  const token = await response.json();
  return token;
};

export const logoutUser = async (accessToken, refreshToken) => {
  const response = await fetch(`${LOGOUT_URL}/${refreshToken}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `${accessToken}`,
    },
  });

  if (!response.ok) {
    const errorData = await response.json();
    throw new Error(errorData);
  }
};

export const requestAccessToken = async (accessToken, refreshToken) => {
  const data = await fetch(`${RESEND_EMAIL_VERIFICATION_URL}/${refreshToken}`, {
    method: 'GET',
    headers: {
      'Authorization': `${accessToken}`,
    },
  });

  if (!data) {
    throw new Error(ERROR_MESSAGE.REQUEST_ACCESS_TOKEN);
  }
};

export const verifyUserAfterSignUp = async (email, token) => {
  const encodedToken = encodeURIComponent(token);
  try {
    const data = await fetch(`${VERIFY_EMAIL_URL}/${email}/${encodedToken}`);
    if (!data.ok) {
      const errorData = await data.json();
      throw new Error(errorData);
    }
  } catch (error) {
    console.error(error);
    throw error.message;
  }
};

export const resendEmailVerification = async (email) => {
  try {
    const data = await fetch(`${RESEND_EMAIL_VERIFICATION_URL}/${email}`);
    if (!data.ok) {
      const errorData = await data.json();
      throw new Error(errorData);
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};
export const sendEmailForResetPassword = async (email) => {
  try {
    const data = await fetch(`${SEND_EMAIL_RESET_PASSWORD}/${email}`);

    if (data.ok) {
      return;
    }

    if (!data.ok) {
      const errorData = await data.json();
      throw new Error(errorData);
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const resetPassword = async (email, token, credentials) => {
  const response = await fetch(`${RESET_PASSWORD_URL}/${email}/${token}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(credentials),
  });

  if (!response.ok) {
    const errorData = await response.json();
    throw new Error(errorData);
  }
};
