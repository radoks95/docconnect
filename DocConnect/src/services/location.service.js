import {ALL_CITIES_URL, ERROR_MESSAGE} from '../common/constants';

export const getAllCities = async () => {
  try {
    const data = await fetch(`${ALL_CITIES_URL}/3000/1`);

    if (!data.ok) {
      throw new Error('Failed to fetch specialties');
    }
    return data.json();
  } catch (error) {
    console.error(ERROR_MESSAGE.CITIES, error);
    throw ERROR_MESSAGE.CITIES;
  }
};

