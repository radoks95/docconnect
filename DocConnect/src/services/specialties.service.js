import {ALL_SPECIALTIES_URL, ERROR_MESSAGE} from '../common/constants';

export const getAllSpecialties = async () => {
  try {
    const data = await fetch(ALL_SPECIALTIES_URL);

    if (!data.ok) {
      throw new Error('Failed to fetch specialties');
    }
    return data.json();
  } catch (error) {
    console.error(ERROR_MESSAGE.SPECIALTIES, error);
    throw ERROR_MESSAGE.SPECIALTIES;
  }
};
