import {
  CREATE_APPOINTMENT_URL,
  DOCTOR_APPOINTMENTS_BY_ID_URL,
  PAST_APPOINTMENTS_BY_ID_URL,
  UPCOMING_APPOINTMENTS_BY_ID_URL,
  DELETE_APPOINTMENT_URL,
} from '../common/constants';

export const getDoctorsAppointmentsById = async (id) => {
  try {
    const data = await fetch(`${DOCTOR_APPOINTMENTS_BY_ID_URL}/${id}`);

    if (!data.ok) {
      const errorData = await data.json();
      throw new Error(errorData);
    }

    return data.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const createAppointment = async (appointmentDetails) => {
  const response = await fetch(CREATE_APPOINTMENT_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(appointmentDetails),
  });
  if (!response.ok) {
    const errorData = await response.json();
    throw new Error(errorData);
  }
};

export const getUpcomingAppointmentsById = async (id) => {
  try {
    const data = await fetch(`${UPCOMING_APPOINTMENTS_BY_ID_URL}/${id}`);

    if (!data.ok) {
      const errorData = await data.json();
      throw new Error(errorData);
    }

    return data.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
};
export const getPastAppointmentsById = async (id) => {
  try {
    const data = await fetch(`${PAST_APPOINTMENTS_BY_ID_URL}/${id}`);

    if (!data.ok) {
      const errorData = await data.json();
      throw new Error(errorData);
    }

    return data.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
};


export const deleteAppointmentById = async (id) => {
  const response = await fetch(`${DELETE_APPOINTMENT_URL}/${id}`, {
    method: 'DELETE',
  });

  if (!response.ok) {
    const errorData = await response.json();
    throw new Error(errorData);
  }
};
