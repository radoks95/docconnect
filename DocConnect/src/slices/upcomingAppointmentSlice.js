import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {getUpcomingAppointmentsById} from '../services/appointments.service';

export const fetchAllUpcomingAppointments = createAsyncThunk(
    'upcomingAppointments/fetchAllUpcomingAppointments',
    async (id) => await getUpcomingAppointmentsById(id),
);

const initialState = {
  upcomingAppointments: [],
  isLoadingUpcomingAppointments: false,
  errorUpcomingAppointments: null,
};

const upcomingAppointmentsSlice = createSlice({
  name: 'upcomingAppointments',
  initialState,
  reducers: {
    setUpcomingAppointments: (state, action) => {
      state.upcomingAppointments = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
        .addCase(fetchAllUpcomingAppointments.pending, (state) => {
          state.isLoadingUpcomingAppointments = true;
          state.errorUpcomingAppointments = null;
        })
        .addCase(fetchAllUpcomingAppointments.fulfilled, (state, action) => {
          state.upcomingAppointments = action.payload;
          state.isLoadingUpcomingAppointments = false;
          state.errorUpcomingAppointments = null;
        })
        .addCase(fetchAllUpcomingAppointments.rejected, (state, action) => {
          state.isLoadingUpcomingAppointments = false;
          state.errorUpcomingAppointments = action.error.message;
        });
  },
});

export const upcomingAppointmentsActions = upcomingAppointmentsSlice.actions;
export default upcomingAppointmentsSlice.reducer;
