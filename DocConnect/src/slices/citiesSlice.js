import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getAllCities} from '../services/location.service';

export const fetchAllCities = createAsyncThunk(
    'cities/fetchAllCities',
    async () => await getAllCities(),
);

const initialState = {
  cities: [],
  isLoadingCities: false,
  errorCities: null,
};

const citiesSlice = createSlice({
  name: 'cities',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
        .addCase(fetchAllCities.pending, (state) => {
          state.isLoadingCities = true;
          state.errorCities = null;
        })
        .addCase(fetchAllCities.fulfilled, (state, action) => {
          state.cities = action.payload;
          state.isLoadingCities = false;
          state.errorCities = null;
        })
        .addCase(fetchAllCities.rejected, (state, action) => {
          state.isLoadingCities = false;
          state.errorCities = action.error.message;
        });
  },
});

export const citiesActions = citiesSlice.actions;
export default citiesSlice.reducer;
