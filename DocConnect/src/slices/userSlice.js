import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  user: {
    id: '',
    email: '',
    expireIn: null,
    fullName: [],
  },

};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
    },
    clearUser: (state) => {
      state.user.id = '';
      state.user.email = '';
      state.user.expireIn = null;
    },
  },
});

export const {setUser, clearUser} = userSlice.actions;

export default userSlice.reducer;
