import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getAllSpecialists} from '../services/specialists.service';

export const fetchAllSpecialists = createAsyncThunk(
    'specialists/fetchAllSpecialists',
    async (arg, {getState}) => {
      const {specialistName, specialty, city, page} = arg;
      // returns previous state of specialists
      const {specialists} = getState();
      const data = await getAllSpecialists(specialistName, specialty, city, 18, page);
      return specialists.specialists.length > 0 ? [...specialists.specialists, ...data] : data;
    },
);

const initialState = {
  specialists: [],
  specialistName: '',
  specialty: '',
  city: '',
  isLoadingSpecialists: false,
  errorSpecialists: null,
};

const specialistsSlice = createSlice({
  name: 'specialists',
  initialState,
  reducers: {
    setSpecialists: (state, action) => {
      state.specialists = action.payload;
    },
    setEmptySpecialists: (state) => {
      state.specialists = [];
    },
    setSpecialistName: (state, action) => {
      state.specialistName = action.payload;
    },
    setSpecialty: (state, action) => {
      state.specialty = action.payload;
    },
    setCity: (state, action) => {
      state.city = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
        .addCase(fetchAllSpecialists.pending, (state) => {
          if (state.specialists.length === 0) {
            state.isLoadingSpecialists = true;
          }
          state.errorSpecialists = null;
        })
        .addCase(fetchAllSpecialists.fulfilled, (state, action) => {
          state.specialists = action.payload;
          state.isLoadingSpecialists = false;
          state.errorSpecialists = null;
        })
        .addCase(fetchAllSpecialists.rejected, (state, action) => {
          state.isLoadingSpecialists = false;
          state.errorSpecialists = action.error.message;
        });
  },
});

export const specialistActions = specialistsSlice.actions;
export default specialistsSlice.reducer;
