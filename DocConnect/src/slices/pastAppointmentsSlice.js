import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {getPastAppointmentsById} from '../services/appointments.service';

export const fetchAllPastAppointments = createAsyncThunk(
    'pastAppointments/fetchAllPastAppointments',
    async (id) => await getPastAppointmentsById(id),
);

const initialState = {
  pastAppointments: [],
  isLoadingPastAppointments: false,
  errorPastAppointments: null,
};

const pastAppointmentsSlice = createSlice({
  name: 'pastAppointments',
  initialState,
  reducers: {
    setUpcomingAppointments: (state, action) => {
      state.pastAppointments = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
        .addCase(fetchAllPastAppointments.pending, (state) => {
          state.isLoadingPastAppointments = true;
          state.errorPastAppointments = null;
        })
        .addCase(fetchAllPastAppointments.fulfilled, (state, action) => {
          state.pastAppointments = action.payload;
          state.isLoadingPastAppointments = false;
          state.errorPastAppointments = null;
        })
        .addCase(fetchAllPastAppointments.rejected, (state, action) => {
          state.isLoadingPastAppointments = false;
          state.errorPastAppointments = action.error.message;
        });
  },
});

export const pastAppointmentsActions = pastAppointmentsSlice.actions;
export default pastAppointmentsSlice.reducer;
