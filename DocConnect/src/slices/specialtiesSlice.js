import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {getAllSpecialties} from '../services/specialties.service';

export const fetchAllSpecialties = createAsyncThunk(
    'specialties/fetchAllSpecialties',
    async () => await getAllSpecialties(),
);

const initialState = {
  specialties: [],
  isLoadingSpecialties: false,
  errorSpecialties: null,
};

const specialtiesSlice = createSlice({
  name: 'specialties',
  initialState,
  reducers: {
    setSpecialties: (state, action) => {
      state.specialties = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
        .addCase(fetchAllSpecialties.pending, (state) => {
          state.isLoadingSpecialties = true;
          state.errorSpecialties = null;
        })
        .addCase(fetchAllSpecialties.fulfilled, (state, action) => {
          state.specialties = action.payload;
          state.isLoadingSpecialties = false;
          state.errorSpecialties = null;
        })
        .addCase(fetchAllSpecialties.rejected, (state, action) => {
          state.isLoadingSpecialties = false;
          state.errorSpecialties = action.error.message;
        });
  },
});

export const {setSpecialties} = specialtiesSlice.actions;
export default specialtiesSlice.reducer;
