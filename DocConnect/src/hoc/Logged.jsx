import {useSelector} from 'react-redux';
import {useNavigate} from 'react-router-dom';
import {LOGIN_PAGE} from '../common/routes';
import {useEffect} from 'react';
import {useShowToast} from '../hooks/useShowToast';

const Logged = ({children}) => {
  const user = useSelector((state) => state.user.user);
  const navigate = useNavigate();
  const showToast = useShowToast();

  useEffect(() => {
    if (!user.id) {
      navigate(LOGIN_PAGE);
      showToast('', 'You need to log in to continue', 'error');
    }
  }, [user.id, navigate]);

  if (!user.id) {
    return null;
  }

  return children;
};

export default Logged;
