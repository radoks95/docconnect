import {useSelector} from 'react-redux';
import {useNavigate} from 'react-router-dom';
import {HOME_PAGE} from '../common/routes';
import {useEffect} from 'react';

const NotLogged = ({children}) => {
  const user = useSelector((state) => state.user.user);
  const navigate = useNavigate();

  useEffect(() => {
    if (user.id) {
      navigate(HOME_PAGE);
    }
  }, [user.id, navigate]);

  if (user.id) {
    return null;
  }

  return children;
};

export default NotLogged;
